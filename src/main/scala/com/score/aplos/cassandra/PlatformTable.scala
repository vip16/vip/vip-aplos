package com.score.aplos.cassandra

import com.datastax.driver.core.{PreparedStatement, ResultSet}
import com.score.aplos.protocol.PlatformGetReply
import com.score.aplos.util.AppLogger

import java.util.{Date, UUID}

object PlatformStatus extends Enumeration {
  type VerifiedStatus = Value
  val REGISTERED: PlatformStatus.Value = Value("REGISTERED")
  val SUBSCRIBED: PlatformStatus.Value = Value("SUBSCRIBED")
  val REJECTED: PlatformStatus.Value = Value("REJECTED")
}

case class Platform
(
  uuid: String = UUID.randomUUID().toString,
  name: String,
  email: String,
  page_url: String,
  billing_details: String,
  status: String,
  api_key: String = "",
  expire_date: Date = null,
  timestamp: Date = new Date
)


trait PlatformTable extends CassandraCluster with AppLogger {

  private lazy val qGetPs: PreparedStatement = session.prepare(
    "SELECT * from mystiko.platforms WHERE uuid = ? LIMIT 1")

  private lazy val qInsertPs: PreparedStatement = session.prepare(
    """
    INSERT INTO mystiko.platforms(
      uuid, name, email, page_url, billing_details, status, api_key,
      expire_date, timestamp
    )
    VALUES (?,?,?,?,?,?,?,?,?)
    """)

  private lazy val qUpdateStatus: PreparedStatement = session.prepare(
    "UPDATE mystiko.platforms SET status = ?, api_key= ?, expire_date = ? WHERE uuid = ?")

  def getPlatform(id: String): Option[PlatformGetReply] = {
    val row = session.execute(qGetPs.bind(id)).one()
    if (row != null) {
      val platform = PlatformGetReply(
        uuid = row.getString("uuid"),
        name = row.getString("name"),
        email = row.getString("email"),
        billing_details = row.getString("billing_details"),
        page_url = row.getString("page_url"),
        expire_date = util.Try(row.getTimestamp("expire_date").toString).getOrElse(""),
        status = row.getString("status"),
        api_key = row.getString("api_key"),
        timestamp = row.getTimestamp("timestamp").toString
      )
      return Some(platform)
    }
    None
  }

  def createPlatform(p: Platform): ResultSet = {
    session.execute(qInsertPs.bind(
      p.uuid, p.name, p.email, p.page_url, p.billing_details, p.status,
      p.api_key, p.expire_date, p.timestamp
    ))
  }

  def updatePlatformStatus
  (
    uuid: String,
    status: String,
    api_key: String = "",
    expire_date: Date = null
  ): ResultSet = {
    session.execute(qUpdateStatus.bind(
      status, api_key, expire_date, uuid
    ))
  }
}
