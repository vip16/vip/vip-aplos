package com.score.aplos.cassandra

import com.datastax.driver.core._
import com.score.aplos.cassandra
import com.score.aplos.protocol.QualificationGetReply
import com.score.aplos.util.AppLogger

import java.util.{Date, UUID}
import scala.collection.JavaConverters._

object VerifiedStatus extends Enumeration {
  type VerifiedStatus = Value
  val notRequested: cassandra.VerifiedStatus.Value = Value("NOT_REQUESTED")
  val requested: cassandra.VerifiedStatus.Value = Value("REQUESTED")
  val verified: cassandra.VerifiedStatus.Value = Value("VERIFIED")
  val rejected: cassandra.VerifiedStatus.Value = Value("REJECTED")
}

case class Qualification
(
  uuid: String = UUID.randomUUID().toString,
  user_id: String,
  claim: String,
  org_id: String,
  typ: String,
  start_date: Date,
  end_date: Date,
  proof_blob_id: List[String],
  verified: String = "NOT_VERIFIED",
  org_reg_id: String,
  valid_until_date: Option[Date],
  verified_time: Option[Date],
  verified_by_user_id: String = "",
  timestamp: Date = new Date
)

trait QualificationTable extends CassandraCluster with AppLogger {
  private lazy val qGetPs: PreparedStatement = session.prepare(
    """
    SELECT * from mystiko.qualifications WHERE uuid = ? LIMIT 1;
    """)

  private lazy val qInsertPs: PreparedStatement = session.prepare(
    """
    INSERT INTO mystiko.qualifications(
      uuid, user_id, claim, org_id, typ, start_date, end_date, proof_blob_id,
      verified, org_reg_id, valid_until_date, verified_time, verified_by_user_id,
      timestamp
    )
    VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)
    """)

  private lazy val qUpdateVerifiedStatus: PreparedStatement = session.prepare(
    """
    UPDATE mystiko.qualifications
      SET verified = ?, valid_until_date = ?, verified_time = ?, verified_by_user_id = ?
      WHERE uuid = ?
    """)

  def getQualification(id: String): Option[QualificationGetReply] = {
    val row = session.execute(qGetPs.bind(id)).one()
    if (row != null) {
      Some(QualificationGetReply(
        uuid = row.getString("uuid"),
        user_id = row.getString("user_id"),
        claim = row.getString("claim"),
        org_id = row.getString("org_id"),
        typ = row.getString("typ"),
        start_date = util.Try(row.getTimestamp("start_date").toString).getOrElse(""),
        end_date = util.Try(row.getTimestamp("end_date").toString).getOrElse(""),
        proof_blob_id = row.getSet("proof_blob_id", classOf[String]).asScala.toList,
        verified = row.getString("verified"),
        org_reg_id = row.getString("org_reg_id"),
        valid_until_date = util.Try(row.getTimestamp("valid_until_date").toString).getOrElse(""),
        verified_time = util.Try(row.getTimestamp("verified_time").toString).getOrElse(""),
        verified_by_user_id = row.getString("verified_by_user_id"),
        timestamp = row.getTimestamp("timestamp").toString
      ))
    } else None
  }

  def createQualification(q: Qualification): ResultSet = {
    session.execute(qInsertPs.bind(
      q.uuid,q.user_id,q.claim,q.org_id,q.typ,q.start_date,q.end_date,
      q.proof_blob_id.toSet.asJava:java.util.Set[String],
      q.verified,q.org_reg_id,q.valid_until_date.getOrElse(null),q.verified_time.getOrElse(null),q.verified_by_user_id,q.timestamp
    ))
  }

  def updateQualificationVerifiedStatus
  (
    uuid: String,
    verified: String,
    valid_until_date: Date = null,
    verified_time: Date = null,
    verified_by_user_id: String = ""
  ): ResultSet = {
    session.execute(qUpdateVerifiedStatus.bind(
      verified, valid_until_date, verified_time, verified_by_user_id, uuid
    ))
  }
}