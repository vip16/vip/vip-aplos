package com.score.aplos.cassandra

import com.score.aplos.util.AppLogger
import com.dataoperandz.cassper.Cassper
import com.datastax.driver.core.{PreparedStatement, ResultSet}

import java.util.{Date, UUID}
import scala.collection.mutable.ListBuffer

case class Trans
(
  id: String,
  execer: String,
  actor: String,
  messageTyp: String,
  message: String,
  digsig: String,
  timestamp: Date = new Date
)

case class Blob
(
  id: String,
  blob: String,
  timestamp: Date = new Date()
)

object CassandraStore extends CassandraCluster with AccountTable with QualificationTable with IdentityTable with PlatformTable with AppLogger {

  // transaction
  lazy val tInsertPs: PreparedStatement = session.prepare(
    """INSERT INTO mystiko.trans(
       id, execer, actor, message_typ, message, digsig, timestamp
      )
      VALUES(?, ?, ?, ?, ?, ?, ?)""")
  lazy val tGetPs: PreparedStatement = session.prepare(
    "SELECT id FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ? LIMIT 1")

  // blob
  lazy val bCreatePs: PreparedStatement = session.prepare(
    "INSERT INTO mystiko.blobs(id, blob, timestamp) VALUES(?, ?, ?)")
  lazy val bUpdatePs: PreparedStatement = session.prepare(
    "UPDATE mystiko.blobs SET blob = ? WHERE id = ?")
  lazy val bGetPs: PreparedStatement = session.prepare(
    "SELECT * FROM mystiko.blobs where id = ? LIMIT 1")

  def init(): Unit = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    // create udts, tables
    val builder = new Cassper().build("mystiko", session)
    builder.migrate("mystiko")

    val admin = Account(
      "admin","ADMIN","admin", "XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=",
      List(), "", "Admin", "21/02/1997", "0112345678", "",
      "", "", "", "1615264771", "454545", 0, "verified", false, List(),
      "verified","verified", true, true)
    CassandraStore.createAccount(admin)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(tGetPs.bind(execer, actor, id)).one()
    row != null
  }

  def createTrans(trans: Trans): ResultSet = {
    session.execute(tInsertPs.bind(trans.id, trans.execer, trans.actor, trans.messageTyp, trans.message,
      trans.digsig, trans.timestamp))
  }

  def createBlob(blob: Blob): ResultSet = {
    session.execute(bCreatePs.bind(blob.id, blob.blob, blob.timestamp))
  }

  def createBlob(blobData: String): Blob = {
    val blob_id = UUID.randomUUID().toString
    val blob = Blob(blob_id, blobData)
    createBlob(blob)
    blob
  }

  def updateBlob(id: String, blob: String): ResultSet = {
    session.execute(bUpdatePs.bind(blob, id))
  }

  def getBlob(id: String): Option[Blob] = {
    val row = session.execute(bGetPs.bind(id)).one()
    if (row != null) Option(Blob(row.getString("id"), row.getString("blob"),
      row.getTimestamp("timestamp")))
    else None
  }

  def getBlobStrings(ids: List[String]): List[String] = {
    var blobs = new ListBuffer[String]()
    ids.foreach(blobId => {
      val blob = getBlob(blobId)
      if (blob.isDefined) {
        blobs += blob.get.blob
      }
    })
    blobs.toList
  }
  def getBlobString(id :String) :String={
    val blob= getBlob(id)
    if (blob != None) {
      blob.get.blob
    }
    else ""
  }
}

//object M extends App {
//  CassandraStore.init()
//  println(CassandraStore.isDoubleSpend("eranga", "AccountActor", "112"))
//
//  val acc1 = Account(
//    "111", "222", "w32323", "user", List(), "pubkey", "2323", "2323", "2323", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  val acc2 = Account(
//    "111", "222", "password", "user", List(), "pubkey", "device_token", "apple", "imei", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  CassandraStore.createAccount(acc1)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.registerAccount(acc2)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.activateAccount("111", "222", true)
//  CassandraStore.verifyAccount("111", "222", true)
//  //    CassandraStore.updateAttempts("775432015", 2)
//  //    CassandraStore.disableAccount("775432015", true)
//  CassandraStore.createTrans(Trans("112", "eranga", "AccountActor", "create", "{\"message\": \"java\"}", "1111"))
//
//  CassandraStore.createTrace(Trace("111", "111", "1212", "ops", "077", "222", "2323", "333", "hak", "matale", "232", "sig", false, 2.121, 4.2323))
//  println(CassandraStore.getTrace("111", "111"))
//
//  CassandraStore.createConsents(Consent("111", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  CassandraStore.createConsents(Consent("222", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  println(CassandraStore.getConsents("111"))
//}
