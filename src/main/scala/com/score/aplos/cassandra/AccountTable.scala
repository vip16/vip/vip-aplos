package com.score.aplos.cassandra

import com.datastax.driver.core.{PreparedStatement, ResultSet}
import com.google.common.collect.ImmutableSet
import com.score.aplos.util.AppLogger

import scala.collection.JavaConverters._
import java.util.Date

case class Account
(
  uuid :String,
  typ: String,
  email: String,
  password: String,
  roles: List[String],
  pub_key: String,
  name: String,
  dob: String,
  phone: String,
  address_line: String,
  address_city: String,
  address_country: String,
  address_zip_code: String,
  profile_blob_id: String,
  salt: String,
  attempts: Int,
  verified: String,
  disabled: Boolean,
  platform_id: List[String],
  address_verified: String,
  name_verified: String,
  email_verified: Boolean,
  uuid_verified :Boolean,
  timestamp: Date = new Date
)

trait AccountTable extends CassandraCluster with AppLogger {
  // account
  lazy val aInsertPs: PreparedStatement = session.prepare(
    """
       INSERT INTO mystiko.accounts(
        uuid,typ, email, password, roles, pub_key, name, dob,phone,
        address_line,address_city ,address_country,address_zip_code ,
        profile_blob_id ,salt, attempts, verified, disabled, platform_id ,
        address_verified , name_verified ,email_verified ,uuid_verified,
        timestamp)
       VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""")

  //  lazy val aaps = session.prepare("UPDATE mystiko.accounts SET activated = ? WHERE email = ? AND typ = ?")
  //  lazy val raps = session.prepare("UPDATE mystiko.accounts SET password = ?, device_type = ?, device_token =?, activated=? WHERE did = ? AND owner = ?")
  lazy val aVerifyEmailPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET email_verified = ? WHERE uuid = ? AND typ = ?")
  lazy val aDisablePs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET disabled = ? WHERE uuid = ? AND typ = ?")
  lazy val aUpdateAttemptsPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET attempts = ? WHERE uuid = ? AND typ = ?")
  lazy val aGetPs: PreparedStatement = session.prepare(
    "SELECT * FROM mystiko.accounts where uuid = ? AND typ = ? LIMIT 1")
  lazy val aUpdateSaltPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET salt = ? WHERE uuid = ? AND typ = ?")
  lazy val aUpdatePasswordPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET password = ? WHERE uuid = ? AND typ = ?")
  lazy val aAddRolePs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET roles = roles + :roles WHERE uuid = :uuid AND typ = :typ ")
  lazy val aAddPlatformPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.accounts SET platform_id = platform_id + :platform_id WHERE uuid = :uuid AND typ = :typ ")


  def createAccount(account: Account): ResultSet = {
    session.execute(aInsertPs.bind(
      account.uuid,
      account.typ,
      account.email,
      account.password,
      account.roles.toSet.asJava: java.util.Set[String],
      account.pub_key,
      account.name,
      account.dob,
      account.phone,
      account.address_line,
      account.address_city,
      account.address_country,
      account.address_zip_code,
      account.profile_blob_id,
      account.salt,
      account.attempts: java.lang.Integer,
      account.verified,
      account.disabled: java.lang.Boolean,
      account.platform_id.toSet.asJava: java.util.Set[String],
      account.address_verified,
      account.name_verified,
      account.email_verified: java.lang.Boolean,
      account.uuid_verified: java.lang.Boolean,
      account.timestamp)
    )
  }


  def getAccount(uuid: String, typ: String): Option[Account] = {
    val row = session.execute(aGetPs.bind(uuid, typ)).one()
    if (row != null) Option(Account(
      row.getString("uuid"),
      row.getString("typ"),
      row.getString("email"),
      row.getString("password"),
      row.getSet("roles", classOf[String]).asScala.toList,
      row.getString("pub_key"),
      row.getString("name"),
      row.getString("dob"),
      row.getString("phone"),
      row.getString("address_line"),
      row.getString("address_city"),
      row.getString("address_country"),
      row.getString("address_zip_code"),
      row.getString("profile_blob_id"),
      row.getString("salt"),
      row.getInt("attempts"),
      row.getString("verified"),
      row.getBool("disabled"),
      row.getSet("platform_id", classOf[String]).asScala.toList,
      row.getString("address_verified"),
      row.getString("name_verified"),
      row.getBool("email_verified"),
      row.getBool("uuid_verified"),
      row.getTimestamp("timestamp"))
    )
    else None
  }

  def activateAccount(uuid: String, typ: String, activated: Boolean): ResultSet = {
    session.execute(aVerifyEmailPs.bind(activated: java.lang.Boolean, uuid, typ))
  }

  def disableAccount(uuid: String, typ: String, disabled: Boolean): ResultSet = {
    session.execute(aDisablePs.bind(disabled: java.lang.Boolean, uuid, typ))
  }

  def updatePassword(uuid: String, typ: String, password: String): ResultSet = {
    session.execute(aUpdatePasswordPs.bind(password, uuid, typ))
  }

  def updateSalt(uuid: String, typ: String, salt: String): ResultSet = {
    session.execute(aUpdateSaltPs.bind(salt, uuid, typ))
  }

  def updateAttempts(uuid: String, typ: String, attempts: Int): ResultSet = {
    session.execute(aUpdateAttemptsPs.bind(attempts: java.lang.Integer, uuid, typ))
  }

  def addRole(uuid: String, typ: String, role: String): ResultSet = {
    session.execute(
      aAddRolePs.bind()
        .setString("uuid", uuid)
        .setString("typ", typ)
        .setSet("roles", ImmutableSet.of(role)))
  }

  def addPlatform(uuid: String, typ: String, platform_id: String): ResultSet = {
    session.execute(
      aAddPlatformPs.bind()
        .setString("uuid", uuid)
        .setString("typ", typ)
        .setSet("platform_id", ImmutableSet.of(platform_id)))
  }
}
