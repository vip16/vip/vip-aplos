package com.score.aplos.cassandra

import com.datastax.driver.core._
import com.score.aplos.util.AppLogger

import java.util.Date
import scala.collection.JavaConverters._

case class Identity(
                     uuid: String,
                     user_id: String,
                     identity_type: String,
                     identification_no: String,
                     verified: String,
                     verified_by_user_id: String,
                     verified_by_org_id: String,
                     proof_blobs_id: List[String],
                     timestamp: Date = new Date()
                   ) //9

trait IdentityTable extends CassandraCluster with AppLogger {
  private lazy val iCreatePs: PreparedStatement = session.prepare(
    "INSERT INTO mystiko.identities" +
      "(uuid,user_id,identity_type,identification_no,verified,verified_by_user_id," +
      "verified_by_org_id,proof_blobs_id,timestamp)" +
      "VALUES(?,?,?,?,?,?,?,?,?)"
  )

  private lazy val iVerifyPs: PreparedStatement = session.prepare(
    "UPDATE mystiko.identities SET " +
      "verified=? , " +
      "verified_by_user_id = ? , " +
      "verified_by_org_id= ? " +
      "WHERE uuid=?"
  )
  private lazy val iGetPs: PreparedStatement = session.prepare(
    "SELECT * FROM mystiko.identities where uuid=? LIMIT 1"
  )
  private lazy val iDeletePs: PreparedStatement = session.prepare(
    "DELETE FROM mystiko.identities" +
      "WHERE uuid=?"
  )


  def createIdentity(identity: Identity): ResultSet = {
    session.execute(iCreatePs.bind(
      identity.uuid,
      identity.user_id,
      identity.identity_type,
      identity.identification_no,
      identity.verified,
      identity.verified_by_user_id,
      identity.verified_by_org_id,
      identity.proof_blobs_id.toSet.asJava: java.util.Set[String],
      identity.timestamp
    ))
  }

  def verifyIdentity(uuid: String, verified: String, verified_by_user_id: String, verified_by_org_id: String): ResultSet = {
    session.execute(iVerifyPs.bind(
      verified,
      verified_by_user_id,
      verified_by_org_id,
      uuid
    ))
  }

  def getIdentity(uuid: String): Option[Identity] = {
    val row = session.execute(iGetPs.bind(uuid)).one()
    if (row != null) Option(Identity(
      row.getString("uuid"),
      row.getString("user_id"),
      row.getString("identity_type"),
      row.getString("identification_no"),
      row.getString("verified"),
      util.Try(row.getString("verified_by_user_id")).getOrElse(""),
      util.Try(row.getString("verified_by_org_id")).getOrElse(""),
      row.getSet("proof_blobs_id", classOf[String]).asScala.toList,
      row.getTimestamp("timestamp"))
    )
    else None
  }

  def delIdentity(uuid: String): ResultSet = {
    session.execute(iDeletePs.bind(uuid))
  }

}