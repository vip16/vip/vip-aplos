package com.score.aplos.elastic

import com.score.aplos.cassandra.Account
import com.score.aplos.elastic.ElasticStore._
import com.score.aplos.util.DateFactory

trait AccountElasticSearch {
  def getAccounts
  (
    terms: List[Criteria] = List(),
    wildcards: List[Criteria] = List(),
    mustFilter: List[Criteria] = List(),
    shouldFilter: List[Criteria] = List(),
    nested: Option[Nested] = None,
    geoFilter: Option[GeoFilter] = None,
    sorts: List[Sort] = List(),
    page: Option[Page] = None,
    range:Option[Range]=None
  ): (Long, List[Account]) =
    Search(
      accountsElasticIndex, accountsElasticDocType, mapToAccount,
      terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page,range
    )

  def mapToAccount(h: java.util.Map[String, AnyRef]): Account =
    Account(
      h.get("uuid").asInstanceOf[String],
      h.get("typ").asInstanceOf[String],
      h.get("email").asInstanceOf[String],
      dehydrate(h.get("password").asInstanceOf[String]),
      toList(h.get("roles")),
      dehydrate(h.get("pub_key").asInstanceOf[String]),
      dehydrate(h.get("name").asInstanceOf[String]),
      dehydrate(h.get("dob").asInstanceOf[String]),
      dehydrate(h.get("phone").asInstanceOf[String]),
      dehydrate(h.get("address_line").asInstanceOf[String]),
      dehydrate(h.get("address_city").asInstanceOf[String]),
      dehydrate(h.get("address_country").asInstanceOf[String]),
      dehydrate(h.get("address_zip_code").asInstanceOf[String]),
      dehydrate(h.get("profile_blob_id").asInstanceOf[String]),
      dehydrate(h.get("salt").asInstanceOf[String]),
      h.get("attempts").asInstanceOf[Int],
      h.get("verified").asInstanceOf[String],
      h.get("disabled").asInstanceOf[Boolean],
      toList(h.get("platform_id")),
      dehydrate(h.get("address_verified").asInstanceOf[String]),
      dehydrate(h.get("name_verified").asInstanceOf[String]),
      h.get("email_verified").asInstanceOf[Boolean],
      h.get("uuid_verified").asInstanceOf[Boolean],
      DateFactory.formatToDate(h.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E, DateFactory.UTC_TIME_ZONE)
    )

}
