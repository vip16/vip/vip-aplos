package com.score.aplos.elastic

import com.score.aplos.elastic.ElasticStore._
import com.score.aplos.protocol.{QualificationGetReply, QualificationListReply}

trait QualificationElasticSearch {

  def getQualifications
  (
    terms: List[Criteria] = List(),
    wildcards: List[Criteria] = List(),
    mustFilter: List[Criteria] = List(),
    shouldFilter: List[Criteria] = List(),
    nested: Option[Nested] = None,
    geoFilter: Option[GeoFilter] = None,
    sorts: List[Sort] = List(),
    page: Option[Page] = None,
    range:Option[Range]=None
  ): (Long, QualificationListReply) = {

    val (hits, objList) = Search(
      qualificationsElasticIndex, qualificationsElasticDocType, mapToQualificationGetReply,
      terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page,range
    )
    (hits, QualificationListReply(objList))
  }

  def mapToQualificationGetReply(h: java.util.Map[String, AnyRef]): QualificationGetReply =
    QualificationGetReply(
      uuid = dehydrate(h.get("uuid").asInstanceOf[String]),
      typ = dehydrate(h.get("typ").asInstanceOf[String]),
      user_id = dehydrate(h.get("user_id").asInstanceOf[String]),
      org_id = dehydrate(h.get("org_id").asInstanceOf[String]),
      claim = dehydrate(h.get("claim").asInstanceOf[String]),
      org_reg_id = dehydrate(h.get("org_reg_id").asInstanceOf[String]),
      start_date = dehydrate(h.get("start_date").asInstanceOf[String]),
      end_date = dehydrate(h.get("end_date").asInstanceOf[String]),
      proof_blob_id = toList(h.get("proof_blob_id")),
      verified = dehydrate(h.get("verified").asInstanceOf[String]),
      valid_until_date = dehydrate(h.get("valid_until_date").asInstanceOf[String]),
      verified_time = dehydrate(h.get("verified_time").asInstanceOf[String]),
      verified_by_user_id = dehydrate(h.get("verified_by_user_id").asInstanceOf[String]),
      timestamp = dehydrate(h.get("timestamp").asInstanceOf[String])
    )

}
