package com.score.aplos.elastic

import com.score.aplos.elastic.ElasticStore._

import com.score.aplos.cassandra.Identity
import com.score.aplos.util.DateFactory

trait IdentityElasticSearch {

  def getIdentities
  (
    terms: List[Criteria] = List(),
    wildcards: List[Criteria] = List(),
    mustFilter: List[Criteria] = List(),
    shouldFilter: List[Criteria] = List(),
    nested: Option[Nested] = None,
    geoFilter: Option[GeoFilter] = None,
    sorts: List[Sort] = List(),
    page: Option[Page] = None,
    range:Option[Range]=None
  ): (Long, List[Identity]) =
    Search(
      identitiesElasticIndex, identitiesElasticDocType, mapToIdentity,
      terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page,range
    )

  def mapToIdentity(h: java.util.Map[String, AnyRef]): Identity =
    Identity(
      dehydrate(h.get("uuid").asInstanceOf[String]),
      dehydrate(h.get("user_id").asInstanceOf[String]),
      dehydrate(h.get("identity_type").asInstanceOf[String]),
      dehydrate(h.get("identification_no").asInstanceOf[String]),
      dehydrate(h.get("verified").asInstanceOf[String]),
      dehydrate(h.get("verified_by_user_id").asInstanceOf[String]),
      dehydrate(h.get("verified_by_org_id").asInstanceOf[String]),
      toList(h.get("proof_blobs_id")),
      DateFactory.formatToDate(h.get("timestamp").asInstanceOf[String], DateFactory.TIMESTAMP_FORMAT_E, DateFactory.UTC_TIME_ZONE)
    )


}
