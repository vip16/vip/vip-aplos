package com.score.aplos.elastic

import com.score.aplos.elastic.ElasticStore._
import com.score.aplos.protocol.{PlatformGetReply, PlatformListReply}

trait PlatformElasticSearch {

  def getPlatforms
  (
    terms: List[Criteria] = List(),
    wildcards: List[Criteria] = List(),
    mustFilter: List[Criteria] = List(),
    shouldFilter: List[Criteria] = List(),
    nested: Option[Nested] = None,
    geoFilter: Option[GeoFilter] = None,
    sorts: List[Sort] = List(),
    page: Option[Page] = None,
    range:Option[Range]=None
  ): (Long, PlatformListReply) = {

    // extract offers from search response
    val (hits, objList) = Search(
      platformsElasticIndex, platformsElasticDocType, mapToPlatformGetReply,
      terms, wildcards, mustFilter, shouldFilter, nested, geoFilter, sorts, page,range
    )
    (hits, PlatformListReply(objList))
  }

  def mapToPlatformGetReply(h: java.util.Map[String, AnyRef]): PlatformGetReply =
    PlatformGetReply(
      uuid = dehydrate(h.get("uuid").asInstanceOf[String]),
      name = dehydrate(h.get("name").asInstanceOf[String]),
      email = dehydrate(h.get("email").asInstanceOf[String]),
      page_url = dehydrate(h.get("page_url").asInstanceOf[String]),
      billing_details = dehydrate(h.get("billing_details").asInstanceOf[String]),
      status = dehydrate(h.get("status").asInstanceOf[String]),
      api_key = dehydrate(h.get("api_key").asInstanceOf[String]),
      expire_date = dehydrate(h.get("expire_date").asInstanceOf[String]),
      timestamp = dehydrate(h.get("timestamp").asInstanceOf[String])
    )

}
