package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

trait CassandraConf {
  val cassandraConf: Config = ConfigFactory.load("cassandra.conf")

  // cassandra config
  lazy val cassandraKeyspace: String = cassandraConf.getString("cassandra.keyspace")
  lazy val cassandraHosts: Seq[String] = cassandraConf.getString("cassandra.hosts").split(",").toSeq
  lazy val cassandraPort: Int = cassandraConf.getInt("cassandra.port")
  lazy val cassandraKeyspaceSchema: String = cassandraConf.getString("cassandra.keyspace-schema")
}
