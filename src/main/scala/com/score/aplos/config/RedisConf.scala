package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

trait RedisConf {
  val redisConf: Config = ConfigFactory.load("redis.conf")

  // redis conf
  lazy val redisHost: String = Try(redisConf.getString("redis.host")).getOrElse("dev.localhost")
  lazy val redisPort: Int = Try(redisConf.getInt("redis.port")).getOrElse(6379)
  lazy val redisKey: String = Try(redisConf.getString("redis.key")).getOrElse("mystiko")
}
