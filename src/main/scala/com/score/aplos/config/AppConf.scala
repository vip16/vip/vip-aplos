package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

/**
  * Load configurations define in application.conf from here
  *
  * @author eranga herath(erangaeb@gmail.com)
  */
trait AppConf {

  // config object
  val appConf: Config = ConfigFactory.load()

  // senzie config
  lazy val serviceMode: String = Try(appConf.getString("service.mode")).getOrElse("DEV")
  lazy val serviceName: String = Try(appConf.getString("service.name")).getOrElse("aplos")
  lazy val servicePort: Int = Try(appConf.getInt("service.port")).getOrElse(8761)

  // keys config
  lazy val keysDir: String = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation: String = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation: String = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")

}
