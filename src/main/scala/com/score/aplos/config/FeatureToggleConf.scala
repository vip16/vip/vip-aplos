package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

trait FeatureToggleConf {
  // config object
  val featureToggleCong: Config = ConfigFactory.load("feature-toggle.conf")

  lazy val useSendSms: Boolean = Try(featureToggleCong.getBoolean("feature-toggle.use-send-sms")).getOrElse(true)
}