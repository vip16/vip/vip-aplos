package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

trait ElasticConf {
  val elasticConf: Config = ConfigFactory.load("elastic.conf")

  lazy val elasticCluster: String = elasticConf.getString("elastic.cluster")

  lazy val transElasticIndex: String = elasticConf.getString("elastic.trans-index")
  lazy val transElasticDocType: String = elasticConf.getString("elastic.trans-doc-type")
  lazy val tracesElasticIndex: String = elasticConf.getString("elastic.traces-index")
  lazy val tracesElasticDocType: String = elasticConf.getString("elastic.traces-doc-type")
  lazy val consentsElasticIndex: String = elasticConf.getString("elastic.consents-index")
  lazy val consentsElasticDocType: String = elasticConf.getString("elastic.consents-doc-type")
  lazy val accountsElasticIndex: String = elasticConf.getString("elastic.accounts-index")
  lazy val accountsElasticDocType: String = elasticConf.getString("elastic.accounts-doc-type")
  lazy val qualificationsElasticIndex: String = elasticConf.getString("elastic.qualifications-index")
  lazy val qualificationsElasticDocType: String = elasticConf.getString("elastic.qualifications-doc-type")
  lazy val identitiesElasticIndex: String = elasticConf.getString("elastic.identities-index")
  lazy val identitiesElasticDocType: String = elasticConf.getString("elastic.identities-doc-type")
  lazy val platformsElasticIndex: String = elasticConf.getString("elastic.platforms-index")
  lazy val platformsElasticDocType: String = elasticConf.getString("elastic.platforms-doc-type")

  lazy val elasticHosts: Seq[String] = elasticConf.getString("elastic.hosts").split(",").toSeq
  lazy val elasticPort: Int = elasticConf.getString("elastic.port").toInt
}