package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

trait SmsConf {

  val smsConf: Config = ConfigFactory.load("sms.conf")

  lazy val smsApi: String = Try(smsConf.getString("sms.api")).getOrElse("https://cpsolutions.dialog.lk/index.php/cbs/sms/send")
  lazy val smsApiKey: String = Try(smsConf.getString("sms.api-key")).getOrElse("14767828958768")

}
