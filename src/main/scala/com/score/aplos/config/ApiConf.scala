package com.score.aplos.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.util.Try

trait ApiConf {

  // config object
  val apiConf: Config = ConfigFactory.load("api.conf")

  // senzie config
  lazy val notificationApi: String = Try(apiConf.getString("api.notification"))
    .getOrElse("http://dev.localhost:8762/api/notifications")

}
