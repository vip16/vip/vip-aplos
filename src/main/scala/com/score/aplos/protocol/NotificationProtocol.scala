package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, JsBoolean, JsObject, JsString, JsValue, RootJsonFormat, _}

trait Fcm

case class Notification(deviceType: String, deviceToken: String, title: String, body: String, message: String)

case class AppleNotification(title: String, body: String, documentId: String)

case class FcmApple(to: String, contentAvailable: Boolean, notification: AppleNotification) extends Fcm

case class AndroidNotification(documentId: String)

case class FcmAndroid(to: String, data: AndroidNotification) extends Fcm


object FcmAndroidProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val androidNotificationFormat: RootJsonFormat[AndroidNotification] = jsonFormat1(AndroidNotification)
  implicit val fcmAndroidFormat: RootJsonFormat[FcmAndroid] = jsonFormat2(FcmAndroid)
}

object FcmAppleProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val appleNotificationFormat: RootJsonFormat[AppleNotification] = jsonFormat3(AppleNotification)

  implicit object FcmAppleFormat extends RootJsonFormat[FcmApple] {
    def write(obj: FcmApple): JsValue = JsObject(
      "to" -> JsString(obj.to),
      "content_available" -> JsBoolean(obj.contentAvailable),
      "notification" -> obj.notification.toJson
    )

    def read(json: JsValue): FcmApple = ???
  }

}

object FcmProtocol extends DefaultJsonProtocol {

  import FcmAndroidProtocol._
  import FcmAppleProtocol._

  implicit object FcmMessageFormat extends RootJsonFormat[Fcm] {
    def write(obj: Fcm): JsValue =
      JsObject((obj match {
        case apple: FcmApple => apple.toJson
        case android: FcmAndroid => android.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): Fcm = ???
  }

}

