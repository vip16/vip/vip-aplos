package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, _}

case class AccountMeta
(
  offset: String,
  limit: String,
  count: String,
  total: String
)

case class AccountGetReply
(
  uuid:String,
  email: String,
  typ: String,
  roles: String,
  pub_key: String,
  name: String,
  dob: String,
  phone: String,
  address_line: String,
  address_city: String,
  address_country: String,
  address_zip_code: String,
  profile_blob: String,
  verified: String,
  disabled: Boolean,
  platform_id: List[String],
  address_verified: String,
  name_verified: String,
  email_verified: Boolean,
  uuid_verified: Boolean,
  timestamp: String
)

case class AccountSearchReply
(
  meta: AccountMeta,
  accounts: List[AccountGetReply]
)


object AccountGetReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val accountFormat: RootJsonFormat[AccountGetReply] = jsonFormat21(AccountGetReply)
  implicit val metaFormat: RootJsonFormat[AccountMeta] = jsonFormat4(AccountMeta)

  implicit object accountSearchReplyFormat extends RootJsonFormat[AccountSearchReply] {
    override def write(obj: AccountSearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("accounts", obj.accounts.toJson)
      )
    }

    override def read(json: JsValue): AccountSearchReply = ???
  }

}

