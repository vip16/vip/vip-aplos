package com.score.aplos.protocol

import com.score.aplos.actor.IdentityActor._
import spray.json._

trait IdentityMessage

object IdentityMessageProtocol extends DefaultJsonProtocol {
  implicit val createdFormat: JsonFormat[Create] = jsonFormat8(Create)
  implicit val verifyFormat: JsonFormat[Verify] = jsonFormat7(Verify)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat9(Search)
  implicit val removeFormat: JsonFormat[Remove] = jsonFormat6(Remove)

  implicit object IdentityMessageFormat extends RootJsonFormat[IdentityMessage] {
    def write(obj: IdentityMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case v: Verify => v.toJson
        case g: Get => g.toJson
        case s: Search => s.toJson
        case r: Remove => r.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")

      }).asJsObject.fields)

    def read(json: JsValue): IdentityMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("verify")) => json.convertTo[Verify]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("search")) => json.convertTo[Search]
        case Seq(JsString("remove")) => json.convertTo[Remove]
        case unrecognized => serializationError(s"json serialization error $unrecognized")

      }

  }

}