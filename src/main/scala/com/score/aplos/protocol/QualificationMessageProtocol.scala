package com.score.aplos.protocol

import com.score.aplos.actor.QualificationActor._
import spray.json._

trait QualificationMessage

object QualificationMessageProtocol extends DefaultJsonProtocol {
  implicit val entryFormat:JsonFormat[Entry] = jsonFormat6(Entry)
  implicit val createFormat: JsonFormat[Create] = jsonFormat10(Create)
  implicit val validateFormat: JsonFormat[Validate] = jsonFormat11(Validate)
  implicit val requestFormat: JsonFormat[Request] = jsonFormat4(Request)
  implicit val cancelFormat: JsonFormat[Cancel] = jsonFormat4(Cancel)
  implicit val verifyFormat: JsonFormat[Verify] = jsonFormat5(Verify)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val getByUserFormat: JsonFormat[GetByUser] = jsonFormat3(GetByUser)
  implicit val getByOrgFormat: JsonFormat[GetByOrg] = jsonFormat4(GetByOrg)
  implicit val createListFormat:JsonFormat[CreateList] = jsonFormat5(CreateList)

  implicit object QualificationMessageFormat extends RootJsonFormat[QualificationMessage] {

    override def read(json: JsValue): QualificationMessage = json.asJsObject.getFields("messageType") match {
      case Seq(JsString("create")) => json.convertTo[Create]
      case Seq(JsString("validate")) => json.convertTo[Validate]
      case Seq(JsString("request")) => json.convertTo[Request]
      case Seq(JsString("cancel")) => json.convertTo[Cancel]
      case Seq(JsString("verify")) => json.convertTo[Verify]
      case Seq(JsString("get")) => json.convertTo[Get]
      case Seq(JsString("getByUser")) => json.convertTo[GetByUser]
      case Seq(JsString("getByOrg")) => json.convertTo[GetByOrg]
      case Seq(JsString("entry"))=>json.convertTo[Entry]
      case Seq(JsString("createList"))=>json.convertTo[CreateList]
      case unknown =>
        serializationError(s"json serialization error in qualification message: $unknown")
    }


    override def write(obj: QualificationMessage): JsValue =
      JsObject((obj match {
        case a: Create => a.toJson
        case a: Validate => a.toJson
        case a: Request => a.toJson
        case a: Cancel => a.toJson
        case a: Verify => a.toJson
        case a: Get => a.toJson
        case a: GetByUser => a.toJson
        case a: GetByOrg => a.toJson
        case a: Entry => a.toJson
        case a: CreateList => a.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)
  }

}
