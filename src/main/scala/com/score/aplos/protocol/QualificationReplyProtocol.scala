package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class QualificationCreateReply( id: String)

case class QualificationGetReply
(
  uuid: String,
  user_id: String,
  claim: String,
  org_id: String,
  typ: String,
  start_date: String,
  end_date: String,
  proof_blob_id: List[String],
  verified: String,
  org_reg_id: String,
  valid_until_date: String,
  verified_time: String,
  verified_by_user_id: String,
  timestamp: String
)

case class QualificationValidityReply(valid: Boolean)

case class QualificationListReply(qualifications: List[QualificationGetReply])

object QualificationReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val qualificationCreateReplyFormat: RootJsonFormat[QualificationCreateReply] =
    jsonFormat1(QualificationCreateReply)

  implicit val qualificationGetReplyFormat: RootJsonFormat[QualificationGetReply] =
    jsonFormat14(QualificationGetReply)

  implicit val qualificationValidityReplyFormat: RootJsonFormat[QualificationValidityReply] =
    jsonFormat1(QualificationValidityReply)

  implicit object QualificationListReplyFormat extends RootJsonFormat[QualificationListReply] {
    override def write(obj: QualificationListReply): JsValue = {
      JsObject(
        ("qualifications", obj.qualifications.toJson)
      )
    }

    override def read(json: JsValue): QualificationListReply = ???
  }

}
