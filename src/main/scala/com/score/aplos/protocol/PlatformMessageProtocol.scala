package com.score.aplos.protocol

import com.score.aplos.actor.PlatformActor.{ConfirmAddPlatform, Get, GetActive, GetAll, Register, Subscribe}
import spray.json._

trait PlatformMessage

object PlatformMessageProtocol extends DefaultJsonProtocol {

  implicit val registerFormat: JsonFormat[Register] = jsonFormat7(Register)
  implicit val subscribeFormat: JsonFormat[Subscribe] = jsonFormat5(Subscribe)
  implicit val getFormat: JsonFormat[Get] = jsonFormat4(Get)
  implicit val getAllFormat: JsonFormat[GetAll] = jsonFormat3(GetAll)
  implicit val getActiveFormat: JsonFormat[GetActive] = jsonFormat3(GetActive)
  implicit  val confirmAddPlatform : JsonFormat[ConfirmAddPlatform] =jsonFormat5(ConfirmAddPlatform)

  implicit object PlatformMessageProtocol extends RootJsonFormat[PlatformMessage] {

    override def read(json: JsValue): PlatformMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("register")) => json.convertTo[Register]
        case Seq(JsString("subscribe")) => json.convertTo[Subscribe]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("getAll")) => json.convertTo[GetAll]
        case Seq(JsString("getActive")) => json.convertTo[GetActive]
        case Seq(JsString("confirmAddPlatform"))=>json.convertTo[ConfirmAddPlatform]
        case unknown =>
          serializationError(s"json serialization error in platform message: $unknown")
      }


    override def write(obj: PlatformMessage): JsValue =
      JsObject((obj match {
        case a: Register => a.toJson
        case a: Subscribe => a.toJson
        case a: Get => a.toJson
        case a: GetAll => a.toJson
        case a: GetActive => a.toJson
        case a: ConfirmAddPlatform => a.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)
  }

}