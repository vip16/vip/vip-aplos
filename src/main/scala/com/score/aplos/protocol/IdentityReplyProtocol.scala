package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, _}


case class IdentityMeta
(
  offset: String,
  limit: String,
  count: String,
  total: String
)

case class IdentityReply
(
  uuid: String,
  user_id: String,
  identity_type: String,
  identification_no: String,
  verified: String,
  verified_by_user_id: String,
  verified_by_org_id: String,
  proof_blobs_id: List[String],
  timestamp: String
)

case class IdentitySearchReply
(
  meta: IdentityMeta,
  identities: List[IdentityReply]
)

object IdentityReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val metaFormat: RootJsonFormat[IdentityMeta] = jsonFormat4(IdentityMeta)
  implicit val identityReplyFormat: RootJsonFormat[IdentityReply] = jsonFormat9(IdentityReply)

  implicit object identitySearchReplyFormat extends RootJsonFormat[IdentitySearchReply] {
    override def write(obj: IdentitySearchReply): JsValue = {
      JsObject(
        ("meta", obj.meta.toJson),
        ("identities", obj.identities.toJson)
      )
    }

    override def read(json: JsValue): IdentitySearchReply = ???
  }

}


