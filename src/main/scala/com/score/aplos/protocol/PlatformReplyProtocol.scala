package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class PlatformGetReply
(
  uuid: String,
  name: String,
  email: String,
  page_url: String,
  billing_details: String,
  status: String,
  api_key: String,
  expire_date: String,
  timestamp: String
)

case class PlatformListReply(platforms: List[PlatformGetReply])

object PlatformReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val platformGetReplyFormat: RootJsonFormat[PlatformGetReply] =
    jsonFormat9(PlatformGetReply)

  implicit object PlatformListReplyFormat extends RootJsonFormat[PlatformListReply] {
    override def write(obj: PlatformListReply): JsValue = {
      JsObject(
        ("platforms", obj.platforms.toJson)
      )
    }

    override def read(json: JsValue): PlatformListReply = ???
  }

}
