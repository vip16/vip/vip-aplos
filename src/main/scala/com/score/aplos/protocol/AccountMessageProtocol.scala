package com.score.aplos.protocol

import com.score.aplos.actor.AccountActor._
import spray.json._

trait AccountMessage

object AccountMessageProtocol extends DefaultJsonProtocol {

  implicit val createFormat: JsonFormat[Create] = jsonFormat16(Create)
  implicit val activateFormat: JsonFormat[Activate] = jsonFormat6(Activate)
  implicit val deactivateFormat: JsonFormat[Deactivate] = jsonFormat5(Deactivate)
  implicit val connectFormat: JsonFormat[Connect] = jsonFormat6(Connect)
  implicit val getFormat: JsonFormat[Get] = jsonFormat5(Get)
  implicit val addRoleFormat: JsonFormat[AddRole] = jsonFormat6(AddRole)
  implicit val addPlatformFormat: JsonFormat[AddPlatform] = jsonFormat6(AddPlatform)
  implicit val searchFormat: JsonFormat[Search] = jsonFormat9(Search)
  implicit val changePasswordFormat: JsonFormat[ChangePassword] = jsonFormat7(ChangePassword)
  implicit val getOrgList : JsonFormat[GetOrgList] = jsonFormat7(GetOrgList)
  implicit val getBlob : JsonFormat[GetBlob] = jsonFormat4(GetBlob)
  implicit object AccountMessageFormat extends RootJsonFormat[AccountMessage] {
    def write(obj: AccountMessage): JsValue =
      JsObject((obj match {
        case c: Create => c.toJson
        case a: Activate => a.toJson
        case d: Deactivate => d.toJson
        case c: Connect => c.toJson
        case g: Get => g.toJson
        case r: AddRole => r.toJson
        case p: AddPlatform => p.toJson
        case s: Search => s.toJson
        case cp: ChangePassword => cp.toJson
        case gol:GetOrgList=>gol.toJson
        case gb :GetBlob=>gb.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): AccountMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[Create]
        case Seq(JsString("activate")) => json.convertTo[Activate]
        case Seq(JsString("deactivate")) => json.convertTo[Deactivate]
        case Seq(JsString("connect")) => json.convertTo[Connect]
        case Seq(JsString("get")) => json.convertTo[Get]
        case Seq(JsString("addRole")) => json.convertTo[AddRole]
        case Seq(JsString("addPlatform")) => json.convertTo[AddPlatform]
        case Seq(JsString("search")) => json.convertTo[Search]
        case Seq(JsString("changePassword")) => json.convertTo[ChangePassword]
        case Seq(JsString("getOrgList")) => json.convertTo[GetOrgList]
        case Seq(JsString("getBlob"))=>json.convertTo[GetBlob]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
