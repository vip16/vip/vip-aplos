package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._

case class Reply(success: Boolean, error: JsValue = JsNull, data: JsValue = JsNull)

object ReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit object replyFormat extends RootJsonFormat[Reply] {
    override def write(obj: Reply): JsValue = {
      JsObject(
        "success" -> JsBoolean(obj.success),
        "error" -> obj.error,
        "data" -> obj.data
      )
    }


    override def read(json: JsValue): Reply = ???
  }

}