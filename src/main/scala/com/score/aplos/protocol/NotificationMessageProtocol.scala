package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class NotificationMessage
(
  traceId: String,
  tracerDid: String,
  tracerOwnerDid: String,
  tracerName: String,
  salt: String
)

object NotificationMessageProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val format: RootJsonFormat[NotificationMessage] = jsonFormat5(NotificationMessage)
}

