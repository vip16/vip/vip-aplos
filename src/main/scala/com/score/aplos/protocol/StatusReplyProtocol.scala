package com.score.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class StatusReply(code: Int, msg: String)

object StatusReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val statusFormat: RootJsonFormat[StatusReply] = jsonFormat2(StatusReply)
}
