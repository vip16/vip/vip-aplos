package com.score.aplos.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.score.aplos.actor.HttpActor.Serve
import com.score.aplos.config.AppConf
import com.score.aplos.protocol._
import com.score.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Serve()

  def props()(implicit system: ActorSystem): Props = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // materializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec: ExecutionContextExecutor = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor): Route = {
    implicit val timeout: Timeout = Timeout(60.seconds)

    import com.score.aplos.protocol.StatusReplyProtocol._
    //    import com.score.aplos.protocol.TraceMessageProtocol._

    pathPrefix("api") {
      path("accounts") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "account contract"))
        } ~
          post {
            import com.score.aplos.protocol.AccountMessageProtocol._

            entity(as[AccountMessage]) {
              case message: AccountMessage =>
                val f = context.actorOf(AccountActor.props()) ? message
                onComplete(f) {
                  any: Any => handleDefaultCases(any)
                }
              case _ =>
                logger.info("Invalid Message, Not matching route")
                complete(StatusCodes.NotFound -> "Route not found")
            }
          } ~
          post {
            import com.score.aplos.protocol.AccountMessageProtocol._

            entity(as[List[AccountMessage]]) {
              case createList: List[AccountActor.Create] =>
                val f = context.actorOf(AccountActor.props()) ? createList
                onComplete(f) {
                  any: Any => handleDefaultCases(any)
                }
            }
          }
      } ~ path("qualifications") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "qualification contract"))
        } ~
          post {
            import com.score.aplos.protocol.QualificationMessageProtocol._

            entity(as[QualificationMessage]) {
              case message: QualificationMessage =>
                val f = context.actorOf(QualificationActor.props()) ? message
                onComplete(f) {
                  any: Any => handleDefaultCases(any)
                }

              case _ =>
                logger.info("Invalid Message, Not matching route")
                complete(StatusCodes.NotFound -> "Route not found")
            }
          }
      } ~ path("identities") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "identities contract"))
        } ~
          post {
            import com.score.aplos.protocol.IdentityMessageProtocol._

            entity(as[IdentityMessage]) {
              case message: IdentityMessage =>
                val f = context.actorOf(IdentityActor.props()) ? message
                onComplete(f) {
                  any: Any => handleDefaultCases(any)
                }

              case _ =>
                logger.info("Invalid Message, Not matching route")
                complete(StatusCodes.NotFound -> "Route not found")
            }
          }
      } ~ path("platforms") {
        get {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "platform contract"))
        } ~
          post {
            import com.score.aplos.protocol.PlatformMessageProtocol._

            entity(as[PlatformMessage]) {
              case message: PlatformMessage =>
                val f = context.actorOf(PlatformActor.props()) ? message
                onComplete(f) {
                  any: Any => handleDefaultCases(any)
                }

              case _ =>
                logger.info("Invalid Message, Not matching route")
                complete(StatusCodes.NotFound -> "Route not found")
            }
          }
      }

    }
  }

  def handleDefaultCases(value: Any): Route = {
    value match {
      case Success(reply: Reply) =>
        logger.debug(s"get Reply $reply")
        import com.score.aplos.protocol.ReplyProtocol._
        complete(reply)

      //token
      case Success(token: TokenReply) =>
        import com.score.aplos.protocol.TokenReplyProtocol._
        logger.info(s"create identity token $token")
        complete(StatusCodes.OK -> token)

      //StatusCode
      case Success(value: StatusReply) =>
        logger.debug(s"received value: $value")
        complete(value.code, value.msg)

      //Failure
      case Failure(e) =>
        logger.debug(s"received error: $e")
        logError(e)
        complete(StatusCodes.BadRequest -> s"Error $e")

      //other
      case unknown: Any =>
        logger.debug(s"received unexpected: $unknown")
        complete(StatusCodes.BadRequest -> s"Unknown error: $unknown")
    }
  }
}
