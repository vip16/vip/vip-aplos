package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.score.aplos.actor.AccountActor._
import com.score.aplos.cassandra.CassandraStore.getBlobString
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf, SmsConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.{Criteria, Page, Range, Sort}
import com.score.aplos.protocol._
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import java.net.URLEncoder
import java.util.{Date, Random}
import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration._
import scala.util.Try

object AccountType extends Enumeration {
  type AccountType = Value
  val admin: AccountType.Value = Value("ADMIN")
  val org: AccountType.Value = Value("ORG")
  val platform: AccountType.Value = Value("PLATFORM")
  val tracer: AccountType.Value = Value("TRACER")
  val user: AccountType.Value = Value("USER")
}

object AccountActor {

  case class Create(
                     messageType: String, execer: String, id: String,
                     uuid:String,
                     email: String,
                     typ: String,
                     password: String,
                     pub_key: String,
                     name: String,
                     dob: String,
                     phone: String,
                     address_line: String,
                     address_city: String,
                     address_country: String,
                     address_zip_code: String,
                     profile_blob: String
                   ) extends AccountMessage //16
  //verify email
  case class Activate(messageType: String, execer: String, id: String,
                      uuid: String,
                      typ: String,
                      salt: String
                     ) extends AccountMessage //6
  //
  case class Deactivate(
                         messageType: String, execer: String, id: String,
                         uuid: String,
                         typ: String
                       ) extends AccountMessage //5

  //login
  case class Connect(
                      messageType: String, execer: String, id: String,
                      uuid: String,
                      typ: String,
                      password: String
                    ) extends AccountMessage //4
  //update pw
  case class ChangePassword(
                             messageType: String, execer: String, id: String,
                             uuid: String,
                             typ: String,
                             oldPassword: String,
                             newPassword: String
                           ) extends AccountMessage //7

  case class AddRole(
                      messageType: String, execer: String, id: String,
                      uuid: String,
                      typ: String,
                      role: String
                    ) extends AccountMessage //6
  case class AddPlatform(
                          messageType: String, execer: String, id: String,
                          uuid: String,
                          typ: String,
                          platform_id: String  //platform_username:platformid
                        ) extends AccountMessage //6


  case class Get(
                  messageType: String, execer: String, id: String,
                  uuid: String,
                  typ: String
                ) extends AccountMessage //5
  case class GetBlob(
                      messageType: String, execer: String, id: String,
                      blobid: String
                    )extends AccountMessage

  case class Search(
                     messageType: String, execer: String, id: String,
                     offset: String,
                     limit: String,
                     uuid: String,
                     typ: String, //USER , ADMIN , TRACER , ORG
                     sort: String = "descending",
                     searchTerm: String //email to search for
                   ) extends AccountMessage //9
  case class GetOrgList(
                         messageType: String, execer: String, id: String,
                         offset: String,
                         limit: String,
                         offsetDate: String, // will fetch organisation registered after this date
                         sort: String = "descending"
                       ) extends AccountMessage //7

  def props()(implicit materializer: ActorMaterializer): Props = Props(new AccountActor)

}

class AccountActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with SmsConf with FeatureToggleConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got create message - $create")

      CassandraStore.getAccount(create.uuid, create.typ) match {
        case Some(acc) =>
          // already existing account
          logger.error(s"activated account already exists - $acc")
          sender ! Reply(false, JsString("User already exists with given uuid and typ."))
        case None =>
          // no activated account exists
          // get execer user
          // execer comes with <user>:<typ>
          val arr = create.execer.split(":")

          CassandraStore.getAccount(arr(0).trim, arr(1).trim) match {
            case Some(eAcc) =>
              // create tracer on behalf of the organization , e.g org A add its employees
              if (eAcc.typ == AccountType.org.toString || eAcc.typ == AccountType.admin.toString) {
                // have permission to create account
                // create trans
                implicit val format: JsonFormat[Create] = jsonFormat16(Create)
                val trans = Trans(create.id, create.execer, "AccountActor", "Create", create.toJson.toString, "digsig")
                CassandraStore.createTrans(trans)

                // create not active account
                val salt = (100000 + new Random().nextInt(900000)).toString
                if (create.profile_blob != null) {
                  val blob = Blob(create.id, create.profile_blob)
                  CassandraStore.createBlob(blob)
                }
                val acc = Account(
                  create.uuid,create.typ,create.email,
                  create.password, List(), create.pub_key, create.name, create.dob, create.phone,
                  create.address_line,  create.address_city, create.address_country,
                  create.address_zip_code, create.id,
                  salt, 0, "unverified", false, List(), "unverified", "accepted", true, true)
                CassandraStore.createAccount(acc)
                //TODO:remove default verifeid setting for ORG TRcer
                logger.info(s"account created - $create")
                sender ! Reply(true, data = JsString("Created Please verify "))
              } else {
                // no permission to create account
                // Unauthorized
                sender ! StatusReply(401, "Unauthorized request.")
              }
            case None =>
              // create normal user, e.g create identity from connect mobile app
              arr(1) match {
                case "USER" =>
                  implicit val format: JsonFormat[Create] = jsonFormat16(Create)
                  val trans = Trans(create.id, create.execer, "AccountActor", "Create", create.toJson.toString, "digsig")
                  CassandraStore.createTrans(trans)

                  // create blob
                  if (create.profile_blob != null) {
                    val blob = Blob(create.id, create.profile_blob)
                    CassandraStore.createBlob(blob)
                  }

                  // create account
                  val salt = (100000 + new Random().nextInt(900000)).toString
                  val acc = Account(
                    create.uuid,AccountType.user.toString,create.email,
                    create.password, List(), create.pub_key, create.name, create.dob, create.phone,
                    create.address_line,  create.address_city, create.address_country,
                    create.address_zip_code, create.id,
                    salt, 0, "unverified", false, List(), "unverified", "accepted", true, true)
                  CassandraStore.createAccount(acc)

                  // token reply
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                  val token = Token(acc.uuid, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                  sender ! Reply(true, data = TokenReply(201, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))
                case _ =>
                  // owner not there
                  logger.error(s"invalid account owner - $create")
                  sender ! StatusReply(401, "Invalid account owner.")
              }
          }
      }
      context.stop(self)
    case getblob:GetBlob =>
      logger.info(s"got get message - $getblob")

      CassandraStore.getBlob(getblob.blobid) match {
        case Some(b) =>
          sender !Reply(true,data = JsString(b.blob))
        case _ =>
          logger.error(s"no matching blob found ")
          sender !Reply(false , data = JsString("No such Blob"))
      }
      context.stop(self)
    case createList: List[Create] =>
      logger.info(s"got account create list message - $createList")

      // create batch
      createList.foreach { create =>
        val salt = (100000 + new Random().nextInt(900000)).toString
        val acc = Account(
          create.uuid,AccountType.user.toString,create.email,
          create.password, List(), create.pub_key, create.name, create.dob, create.phone,
          create.address_line,  create.address_city, create.address_country,
          create.address_zip_code, create.id,
          salt, 0, "unverified", false, List(), "unverified", "accepted", true, true)
        CassandraStore.createAccount(acc)
      }

      sender ! Reply(true, data = JsString("Created"))
      context.stop(self)

    case activate: Activate =>
      logger.info(s"got activate message - $activate")

      // first check double spend
      val id = s"${activate.execer};AccountActor;${activate.id}"
      if (!CassandraStore.isDoubleSpend(activate.execer, "AccountActor", activate.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(activate.uuid, activate.typ) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.attempts > 3) {
              // no existing account
              logger.error(s"exceed account activate attempts - $acc")
              CassandraStore.disableAccount(acc.uuid, acc.typ, true)
              sender ! Reply(false, JsString("Failed to activate account, exceed activate attempts."))
            } else {
              // compare salt
              if (acc.salt.equals(activate.salt)) {
                // valid salt
                // create trans
                implicit val format: JsonFormat[Activate] = jsonFormat6(Activate)
                val trans = Trans(activate.id, activate.execer, "AccountActor", "Activate", activate.toJson.toString, "")
                CassandraStore.createTrans(trans)

                // update attempts
                // activate account
                CassandraStore.updateAttempts(acc.uuid, acc.typ, acc.attempts + 1)
                CassandraStore.activateAccount(acc.uuid, acc.typ, activated = true)

                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                val token = Token(acc.uuid, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                sender ! Reply(true, data = TokenReply(200, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))
              } else {
                // no existing account
                logger.error(s"invalid salt to activate account - $acc")
                CassandraStore.updateAttempts(acc.uuid, acc.typ, acc.attempts + 1)

                sender ! Reply(false, JsString("Verification code is incorrect. Please enter correct Verification code."))
              }
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${activate.uuid}, ${activate.typ}")

            sender ! StatusReply(401, "Failed to activate account, your uuid is not registered with VIP")
        }
      } else {
        logger.error(s"double spend activate $activate")

        sender ! Reply(false, JsString("double spend"))
      }

      context.stop(self)

    case deactivate: Deactivate =>
      logger.info(s"got activate message - $deactivate")

      val arr = deactivate.execer.split(":")
      // first check double spend
      val id = s"${deactivate.execer};AccountActor;${deactivate.id}"
      if (!CassandraStore.isDoubleSpend(deactivate.execer, "AccountActor", deactivate.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(deactivate.uuid, deactivate.typ) match {
          case Some(acc) =>
            // check account activate attempts
            if ((acc.roles.contains(arr(0)) && arr(1).equalsIgnoreCase(AccountType.org.toString)) || arr(1).equalsIgnoreCase(AccountType.admin.toString)) {
              // no existing account
              logger.error(s"account deactivated - $acc")
              implicit val format: JsonFormat[Deactivate] = jsonFormat5(Deactivate)
              val trans = Trans(deactivate.id, deactivate.execer, "AccountActor", "Deactivate", deactivate.toJson.toString, "")
              CassandraStore.createTrans(trans)
              CassandraStore.activateAccount(acc.uuid, acc.typ, false)
              sender ! Reply(true, data = JsString("Acc deactivated"))
            } else {
              //NO access
              logger.error(s"excecer {${deactivate.execer}} has no access to  - $acc")
              sender ! Reply(false, JsString("No access"))
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${deactivate.uuid}, ${deactivate.typ}")
            sender ! StatusReply(401, "Failed to deactivate account, your uuid is not registered with VIP")
        }
      } else {
        logger.error(s"double spend deactivate $deactivate")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)

    case changePassword: ChangePassword =>
      logger.info(s"got changePassword message - $changePassword")

      // first check double spend
      val id = s"${changePassword.execer};AccountActor;${changePassword.id}"
      if (!CassandraStore.isDoubleSpend(changePassword.execer, "AccountActor", changePassword.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(changePassword.uuid, changePassword.typ) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.email_verified) {
              if (acc.password.equals(changePassword.oldPassword)) {
                implicit val format: JsonFormat[ChangePassword] = jsonFormat7(ChangePassword)
                val trans = Trans(changePassword.id, changePassword.execer, "AccountActor", "ChangePassword", changePassword.toJson.toString, "")
                CassandraStore.createTrans(trans)
                CassandraStore.updatePassword(changePassword.uuid, changePassword.typ, changePassword.newPassword)
                // token reply
                implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                val token = Token(acc.uuid, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                sender ! Reply(true, data = TokenReply(200, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))
              } else {
                logger.error(s"invalid password - $acc")
                sender ! Reply(false, JsString("Failed to change password, current password is incorrect."))
              }
            } else {
              // not activated acc
              logger.error(s"account not activated - $acc")
              sender ! StatusReply(401, "Failed to change password, your account is not activated.")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${changePassword.uuid}")
            sender ! StatusReply(401, "Failed to change the password, you are not not registered in VIP.")
        }
      } else {
        logger.error(s"double spend activate $changePassword")
        sender ! Reply(false, JsString("Double spend"))
      }

      context.stop(self)

    case connect: Connect =>
      logger.info(s"got connect account message - $connect")

      // first check double spend
      val id = s"${connect.execer};AccountActor;${connect.id}"
      if (!CassandraStore.isDoubleSpend(connect.execer, "AccountActor", connect.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(connect.uuid, connect.typ) match {
          case Some(acc) =>
            // check account activate attempts
            if (acc.email_verified) {
              if (acc.password.equals(connect.password)) {
                  // all done, token reply with login done
                  implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
                  val token = Token(acc.uuid, acc.roles.mkString(","), DateFactory.timestamp(), 60)
                  sender ! Reply(true, data = TokenReply(200, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))

              } else {
                logger.error(s"invalid password - $acc")
                CassandraStore.updateAttempts(acc.uuid, acc.typ, acc.attempts + 1)

                sender ! Reply(false, JsString("Password or User ID is Incorrect."))
              }
            } else {
              // not activated acc
              logger.error(s"account not activated - $acc")

              // token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
              val token = Token(acc.uuid, acc.roles.mkString(","), DateFactory.timestamp(), 60)
              sender ! Reply(true, data = TokenReply(402, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${connect.uuid}, ${connect.typ}")
            sender ! StatusReply(401, "Password or User ID is incorrect.")
        }
      } else {
        logger.error(s"double spend activate $connect")
        sender ! Reply(false, JsString("Double spend"))
      }

      context.stop(self)

    case get: Get =>
      logger.info(s"got get message - $get")
//      TODO:check excecesr
      CassandraStore.getAccount(get.uuid, get.typ) match {
        case Some(a) =>
          val blob = for (b <- CassandraStore.getBlob(a.profile_blob_id)) yield b.blob

          import com.score.aplos.protocol.AccountGetReplyProtocol.accountFormat
          sender ! Reply(true, data = AccountGetReply(
            a.uuid,
            a.email,
            a.typ,
            a.roles.mkString(","),
            a.pub_key,
            a.name,
            a.dob,
            a.phone,
            a.address_line,
            a.address_city,
            a.address_country,
            a.address_zip_code,
            blob.getOrElse(""),
            a.verified,
            a.disabled,
            a.platform_id,
            a.address_verified,
            a.name_verified,
            a.email_verified,
            a.uuid_verified,
            DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse("")
          ).toJson)
        case _ =>
          logger.error(s"no matching account found - ${get.uuid}, ${get.typ}")
          sender ! StatusReply(404, "Failed to get account, account not found")
      }

      context.stop(self)
    case addRole: AddRole =>
      logger.info(s"got addRole message - $addRole")

      // first check double spend
      val id = s"${addRole.execer};AccountActor;${addRole.id}"
      if (!CassandraStore.isDoubleSpend(addRole.execer, "AccountActor", addRole.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(addRole.uuid, addRole.typ) match {
          case Some(acc) =>
            // check account activate state
            if (!acc.email_verified) {
              // account not activated
              logger.error(s"Account not activated - $acc")
              sender ! Reply(false, JsString("Failed to add role,  account not activated"))
            } else {
              // create trans
              implicit val format: JsonFormat[AddRole] = jsonFormat6(AddRole)
              val trans = Trans(addRole.id, addRole.execer, "AccountActor", "AddRole", addRole.toJson.toString, "")
              CassandraStore.createTrans(trans)

              // add role
              CassandraStore.addRole(acc.uuid, acc.typ, addRole.role)

              // token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
              val token = Token(acc.uuid, acc.roles.mkString(",") + addRole.role, DateFactory.timestamp(), 60)
              sender ! Reply(true, data = JsString("Role Added"))
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${addRole.uuid}, ${addRole.typ}")
            sender ! StatusReply(401, "Failed to activate account, your uuid is not registered with VIP")
        }
      } else {
        logger.error(s"double spend addRole $addRole")
        sender ! Reply(false, JsString("double spend"))
      }

      context.stop(self)

    case addPlatform: AddPlatform =>
      logger.info(s"got addPlatform message - $addPlatform")

      // first check double spend
      val id = s"${addPlatform.execer};AccountActor;${addPlatform.id}"
      if (!CassandraStore.isDoubleSpend(addPlatform.execer, "AccountActor", addPlatform.id) && RedisStore.set(id)) {
        CassandraStore.getAccount(addPlatform.uuid, addPlatform.typ) match {
          case Some(acc) =>
            // check account activate state
            if (!acc.email_verified) {
              // account not activated
              logger.error(s"Account not activated - $acc")
              sender ! Reply(false, JsString("Failed to add role,  account not activated"))
            } else {
//              / token reply
              implicit val tokenFormat: JsonFormat[Token] = jsonFormat5(Token.apply)
              val token = Token(acc.uuid + ":" + acc.typ, addPlatform.platform_id, DateFactory.timestamp(), 120)

              sender ! Reply(true, data = TokenReply(201, CryptoFactory.encode(token.toJson.toString)).toJson(jsonFormat2(TokenReply)))
              logger.info("token sent")
            }
          case None =>
            // no existing account
            logger.error(s"no account found for - ${addPlatform.uuid}, ${addPlatform.typ}")
            sender ! StatusReply(401, "Failed to activate account, your email is not registered with VIP")
        }
      } else {
        logger.error(s"double spend addPlatform $addPlatform")
        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)


    case search: Search =>
      logger.info(s"got search message - $search")

      // check execer account and roles
      val arr = search.execer.split(":")
      CassandraStore.getAccount("admin", "ADMIN") match {
        case Some(acc) =>
          logger.info(s"got search message platform - $acc")

          // sorts and pages
          val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false)) // sort in descending order by timestamp
          val page = Page(Try(search.offset.toInt).getOrElse(0), Try(search.limit.toInt).getOrElse(10))


          //filters
          var mustFilter = ListBuffer[Criteria]()
          //if normal user only return his identities
          if (acc.typ.equalsIgnoreCase(AccountType.user.toString)) {
            mustFilter += Criteria("uuid", arr(0))
            mustFilter += Criteria("typ", AccountType.user.toString)
          } else if (acc.typ.equalsIgnoreCase(AccountType.org.toString)) {
            mustFilter += Criteria("typ", AccountType.tracer.toString)
            //TODO : Add filter for roles
          }

          val mf = if (mustFilter.isEmpty) List() else mustFilter.toList
          logger.info(s"${mf.toString}")

          val (hits, accounts) = ElasticStore.getAccounts(List(), List(), mf, List(), None, None, sorts, Option(page))
          val meta = AccountMeta(search.offset, search.limit, accounts.size.toString, hits.toString)
          val searchReply = AccountSearchReply(meta, accounts.map(a =>
            AccountGetReply(
              a.uuid,
              a.email,
              a.typ,
              a.roles.mkString(","),
              a.pub_key,
              a.name,
              a.dob,
              a.phone,
              a.address_line,
              a.address_city,
              a.address_country,
              a.address_zip_code,
              getBlobString(a.profile_blob_id),
              a.verified,
              a.disabled,
              a.platform_id,
              a.address_verified,
              a.name_verified, a.email_verified,
              a.uuid_verified,
              DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse(""))))

          import com.score.aplos.protocol.AccountGetReplyProtocol.accountSearchReplyFormat
          sender ! Reply(true, data = searchReply.toJson)
        case None =>
          logger.error(s"not registered execer - ${search.execer}")
          sender ! StatusReply(401, "Unauthorized execer")
      }

      context.stop(self)
    case gol: GetOrgList =>
      logger.info(s"got gol message - $gol")

      // check execer account and roles
      val arr = gol.execer.split(":")
      arr(1) match {
        case "PLATFORM"=>
          CassandraStore.getPlatform(arr(0).trim()) match {
            case Some(acc) =>
              logger.info(s"got gol message platform - $acc")

              acc.status match {
                case "REGISTERED" =>
                  logger.info(s"${gol.execer} not subscribed")
                  sender ! Reply(true, data = JsString("Platform not subscribed or subscription expired"))
                case "REJECTED" =>
                  logger.info(s"${gol.execer} registraion rew rejected")
                  sender ! Reply(true, data = JsString("Platform rejected pls contact VIP admin"))
                case "SUBSCRIBED" =>
                  // sorts and pages
                  val sorts = List(Sort("timestamp", if (gol.sort.equalsIgnoreCase("ascending")) true else false)) // sort in dessending order by timestamp
                  val page = Page(Try(gol.offset.toInt).getOrElse(0), Try(gol.limit.toInt).getOrElse(10))

                  //filters
                  var RangeFilter = ListBuffer[Range]()
                  //if normal user only return his identities

                  if (gol.offsetDate.nonEmpty) {
                    logger.info(s"getting all org from beginning")
                    RangeFilter += Range(
                      "timestamp",
                      gol.offsetDate,
                      DateFactory.formatToString(Option(new Date), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse("")
                    )
                  }
                  val rf = if (RangeFilter.isEmpty) List() else RangeFilter.toList

                  //filters
                  var mustFilter = ListBuffer[Criteria]()
                  //if normal user only return his identities

                  mustFilter += Criteria("typ", AccountType.org.toString)


                  val mf = if (mustFilter.isEmpty) List() else mustFilter.toList
                  logger.info(s"${mf.toString}")


                  val (hits, accounts) = ElasticStore.getAccounts(List(), List(), mf, List(), None, None, sorts, Option(page), rf.headOption)
                  val meta = AccountMeta(gol.offset, gol.limit, accounts.size.toString, hits.toString)
                  val searchReply = AccountSearchReply(meta, accounts.map(a =>
                    AccountGetReply(
                      a.uuid,
                      a.email,
                      a.typ,
                      a.roles.mkString(","),
                      a.pub_key,
                      a.name,
                      a.dob,
                      a.phone,
                      a.address_line,
                      a.address_city,
                      a.address_country,
                      a.address_zip_code,
                      getBlobString(a.profile_blob_id),
                      a.verified,
                      a.disabled,
                      a.platform_id,
                      a.address_verified,
                      a.name_verified, a.email_verified,
                      a.uuid_verified,
                      DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse(""))))

                  import com.score.aplos.protocol.AccountGetReplyProtocol.accountSearchReplyFormat
                  sender ! Reply(true, data = searchReply.toJson)
                case _ =>
                  logger.info(s"platform status invalid ${acc.status}")
                  sender ! Reply(true ,data=JsString("Invalid Platform status"))

              }
            case None =>
              logger.error(s"not registered execer - ${gol.execer}")
              sender ! StatusReply(401, "Unauthorized execer")
          }
        case "ORG" =>
          CassandraStore.getAccount(arr(0),AccountType.org.toString) match {
            case Some(acc) =>
              logger.info(s"got gol message from user $acc")
              if (acc.disabled){
                logger.info(s"account disable $acc")
                context.stop(self)
              }else {
                sender ! Reply(true, data = JsString("not authorized"))
              }
            case None =>
              logger.error(s"not registered execer - ${gol.execer}")
              sender ! StatusReply(401, "Unauthorized execer")
          }
        case "ADMIN" =>
          CassandraStore.getAccount(arr(0),AccountType.admin.toString) match {
            case Some(acc) =>
              logger.info(s"got gol message from admin $acc")
              if (acc.disabled){
                logger.info(s"account disable $acc")
                sender ! Reply(false, data = JsString("not authorized"))
                context.stop(self)
              }else {
                // sorts and pages
                val sorts = List(Sort("name", if (gol.sort.equalsIgnoreCase("descending")) false else true)) // sort by name in ascending
                val page = Page(Try(gol.offset.toInt).getOrElse(0), Try(gol.limit.toInt).getOrElse(100))
                import com.score.aplos.protocol.AccountGetReplyProtocol
                //filters
                var mustFilter = ListBuffer[Criteria]()
                //if normal user only return his identities

                mustFilter += Criteria("typ", AccountType.org.toString)


                val mf = if (mustFilter.isEmpty) List() else mustFilter.toList
                logger.info(s"${mf.toString}")
                val (hits, accounts) = ElasticStore.getAccounts(sorts = sorts, mustFilter=mf,page = Option(page))
                val meta = AccountMeta(gol.offset, gol.limit, accounts.size.toString, hits.toString)
                val searchReply = AccountSearchReply(meta, accounts.map(a =>
                  AccountGetReply(
                    a.uuid,
                    a.email,
                    a.typ,
                    a.roles.mkString(","),
                    a.pub_key,
                    a.name,
                    a.dob,
                    a.phone,
                    a.address_line,
                    a.address_city,
                    a.address_country,
                    a.address_zip_code,
                    getBlobString(a.profile_blob_id),
                    a.verified,
                    a.disabled,
                    a.platform_id,
                    a.address_verified,
                    a.name_verified, a.email_verified,
                    a.uuid_verified,
                    DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse(""))
                )
                )
                import com.score.aplos.protocol.AccountGetReplyProtocol.accountSearchReplyFormat
                sender ! Reply(true, data = searchReply.toJson)
              }
            case None =>
              logger.error(s"not registered execer - ${gol.execer}")
              sender ! StatusReply(401, "Unauthorized execer")
          }
        case "USER"  =>
          CassandraStore.getAccount(arr(0),AccountType.user.toString) match {
            case Some(acc) =>
              logger.info(s"got gol message from user $acc")
              if (acc.disabled){
                logger.info(s"account disable $acc")
                context.stop(self)
              }else {
                // sorts and pages
                val sorts = List(Sort("name", if (gol.sort.equalsIgnoreCase("descending")) false else true)) // sort by name in ascending
                val page = Page(Try(gol.offset.toInt).getOrElse(0), Try(gol.limit.toInt).getOrElse(100))
                import com.score.aplos.protocol.AccountGetReplyProtocol

                //filters
                var mustFilter = ListBuffer[Criteria]()
                //if normal user only return his identities

                mustFilter += Criteria("typ", AccountType.org.toString)


                val mf = if (mustFilter.isEmpty) List() else mustFilter.toList
                logger.info(s"${mf.toString}")

                val (hits, accounts) = ElasticStore.getAccounts(sorts = sorts, mustFilter=mf,page = Option(page))
                val meta = AccountMeta(gol.offset, gol.limit, accounts.size.toString, hits.toString)
                val searchReply = AccountSearchReply(meta, accounts.map(a =>
                  AccountGetReply(
                    a.uuid,
                    "",
                    "",
                    "",
                    "",
                    a.name,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    getBlobString(a.profile_blob_id),
                    a.verified,
                    a.disabled,
                    List(),
                    a.address_verified,
                    a.name_verified, a.email_verified,
                    a.uuid_verified,
                    DateFactory.formatToString(Option(a.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse(""))
                )
                )
                import com.score.aplos.protocol.AccountGetReplyProtocol.accountSearchReplyFormat
                sender ! Reply(true, data = searchReply.toJson)
              }
            case None =>
              logger.error(s"not registered execer - ${gol.execer}")
              sender ! StatusReply(401, "Unauthorized execer")
              }
      }

      context.stop(self)
  }

  def sendSms(sms: Sms): Int = {
    logger.info(s"post sms $sms, api $smsApi, use-send-sms $useSendSms")

    val msg = URLEncoder.encode(sms.msg, "UTF-8").replace("+", "%20")
    val uri = s"$smsApi?q=$smsApiKey&destination=${sms.phone}&message=$msg"

    if (useSendSms) {
      implicit val ex: ExecutionContextExecutor = context.dispatcher
      val future = Http(context.system).singleRequest(
        HttpRequest(
          HttpMethods.GET,
          uri
        )
      ).flatMap { response =>
        Unmarshal(response.entity).to[String]
      }.map { responseStr =>
        logger.info(s"got response $responseStr")
        responseStr match {
          case "0" =>
            200
          case _ =>
            400
        }
      }.recover {
        case e =>
          logError(e)
          400
      }
      Await.result(future, 60.seconds)
    } else {
      200
    }
  }
}
