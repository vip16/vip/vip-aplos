package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.PlatformActor._
import com.score.aplos.cassandra._
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf, SmsConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.Criteria
import com.score.aplos.protocol.{PlatformListReply, PlatformMessage, Reply, StatusReply, Token, Token1}
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.{AppLogger, CryptoFactory, DateFactory}
import spray.json.DefaultJsonProtocol._
import spray.json._

import java.util.Date
import scala.collection.mutable.ListBuffer

object PlatformActor {
  val PLATFORM_ACTOR_NAME = "PlatformActor"

  case class Register
  (
    messageType: String,
    execer: String,
    id: String,
    name: String,
    email: String,
    page_url: String,
    billing_details: String
  ) extends PlatformMessage

  case class Subscribe
  (
    messageType: String,
    execer: String,
    id: String,
    platform_id: String,
    is_accepted: Boolean
  ) extends PlatformMessage

  case class Get
  (
    messageType: String,
    execer: String,
    id: String,
    platform_id: String
  ) extends PlatformMessage

  case class GetAll
  (
    messageType: String,
    execer: String,
    id: String
  ) extends PlatformMessage

  case class GetActive
  (
    messageType: String,
    execer: String,
    id: String
  ) extends PlatformMessage

  case class ConfirmAddPlatform(
    messageType : String,
    execer:String,  //platform_id:PLATFORM
    id:String,
    token:String,  //base64encoded token
    email:String   //platform_username:PlatformId
  ) extends  PlatformMessage

  def props()(implicit materializer: ActorMaterializer): Props = Props(new PlatformActor())
}

class PlatformActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with SmsConf with FeatureToggleConf with AppLogger {
  override def receive: Receive = {
    case message: GetAll =>
      logger.debug(s"got platform getAll message $message")
      checkAccess(message.execer, Set())

      val (hits, res) = ElasticStore.getPlatforms()
      logger.debug(s"hits: $hits\n Platforms: $res")
      import com.score.aplos.protocol.PlatformReplyProtocol.PlatformListReplyFormat
      sender ! Reply(true, data = res.toJson)
      context.stop(self)

    case message: GetActive =>
      logger.debug(s"got platform getActive message $message")
      checkAccess(message.execer, Set(AccountType.user.toString))

      val mustFilter = List(Criteria("status", PlatformStatus.SUBSCRIBED.toString))
      val (hits, res) = ElasticStore.getPlatforms(mustFilter = mustFilter)
      logger.debug(s"hits: $hits\n Platforms: $res")
      val processed = res.platforms.map(
        item => item.copy(email = "", page_url = "", billing_details = "", api_key = "", timestamp = "")
      )

      import com.score.aplos.protocol.PlatformReplyProtocol.PlatformListReplyFormat
      sender ! Reply(true, data = PlatformListReply(processed).toJson)
      context.stop(self)

    case message: Get =>
      logger.debug(s"got platform get message $message")
      checkAccess(message.execer, Set())

      val pGet = CassandraStore.getPlatform(message.platform_id)
      if (pGet.isEmpty) {
        sender ! Reply(false, JsString("Platform not found"))
        context.stop(self)
      }
      val p = pGet.get
      import com.score.aplos.protocol.PlatformReplyProtocol.platformGetReplyFormat
      sender ! Reply(true, data = p.toJson)
      context.stop(self)

    case message: Register =>
      logger.debug(s"got platform register message $message")

      val id = s"${message.execer};PlatformActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer,"PlatformActor",message.id)&& RedisStore.set(id)) {

        var mustFilter = ListBuffer[Criteria]()
        mustFilter+=Criteria("email",message.email)
        val mf =mustFilter.toList

        val (hits,platforms) = ElasticStore.getPlatforms(mustFilter = mf)
        if (hits>0){
          logger.error(s"platform already registered $message")
          sender ! Reply(false ,JsString("Platform already registered"))
        }else {
          import com.score.aplos.protocol.PlatformMessageProtocol._
          val trans = Trans(message.id, message.execer, PLATFORM_ACTOR_NAME, "register", message.toJson.toString, "digsig")
          CassandraStore.createTrans(trans)

          CassandraStore.createPlatform(Platform(
            name = message.name,
            email = message.email,
            page_url = message.page_url,
            billing_details = message.billing_details,
            status = PlatformStatus.REGISTERED.toString
          ))
          sender ! Reply(true, data = JsString("Platform Registered Successfully"))
        }
      } else {
      logger.error(s"double spend register $message")
      sender ! Reply(false,JsString("double spend"))
      }
      context.stop(self)

    case message: Subscribe =>
      logger.debug(s"got platform subscribe message $message")
      val id = s"${message.execer};PlatformActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer,"PlatformActor",message.id)&& RedisStore.set(id)) {

        checkAccess(message.execer, Set())
        import com.score.aplos.protocol.PlatformMessageProtocol._
        val trans = Trans(message.id, message.execer, PLATFORM_ACTOR_NAME, "subscribe", message.toJson.toString, "digsig")
        CassandraStore.createTrans(trans)

        val p = CassandraStore.getPlatform(message.platform_id)
        if (p.isEmpty) {
          sender ! Reply(false, JsString("Platform not found"))
          context.stop(self)
        }
        if (message.is_accepted) {
          CassandraStore.updatePlatformStatus(
            message.platform_id, PlatformStatus.SUBSCRIBED.toString, "api-key", new Date()
          )
        } else {
          CassandraStore.updatePlatformStatus(
            message.platform_id, PlatformStatus.REJECTED.toString
          )
        }

        sender ! Reply(true, data = JsString("Action completed successfully"))
      } else {
        logger.error(s"double spend register $message")
        sender ! Reply(false,JsString("double spend"))
      }
      context.stop(self)
    case message: ConfirmAddPlatform=>
      logger .debug(s"got ConfirmPlatformAdd message $message")
      val id = s"${message.execer};PlatformActor;${message.id}"

      if (!CassandraStore.isDoubleSpend(message.execer,"PlatformActor",message.id)&& RedisStore.set(id)) {
        val arr = message.execer.split(":")
        CassandraStore.getPlatform(arr(0).trim()) match {
          case Some(p) =>
            logger.info(s"got ${message.messageType} from ${p.uuid}")
            try {
              val tok = message.token
              logger.info(s"token is $tok")
              val decodedJson = CryptoFactory.decode(tok)
              logger.info(s"decoded token is $decodedJson")
              decodedJson.asJsObject.getFields("id", "roles", "ttl", "issueTime", "digsig") match {
                case Seq(idJ, rolesJ, ttlJ, issueTimeJ, digsigJ) =>
                  val id = idJ.toString.stripPrefix("\"").stripSuffix("\"")
                  val roles = rolesJ.toString.stripPrefix("\"").stripSuffix("\"")
                  val ttl = ttlJ.toString.stripPrefix("\"").stripSuffix("\"")
                  val issueTime = issueTimeJ.toString.stripPrefix("\"").stripSuffix("\"")
                  val digSig = digsigJ.toString.stripPrefix("\"").stripSuffix("\"")

                  logger.debug(s"token info $id $roles $ttl $issueTime $digSig")
                  val arr1 = roles.split(":") //platformUname:PlatformID
                  val arr2 = id.split(":") //user email : type
                  if (!arr1(1).equals(arr(0))) {
                    var arr11=arr1(1)
                    var arr01=arr(0)
                    logger.debug(s"token not authorized for ${message.execer}  arr1 $arr11 arr2m $arr01")
                    sender ! Reply(false, JsString("Unauthorized"))
                  }else {
                    val dsig = CryptoFactory.sign(s"$id$roles$issueTime$ttl".toLowerCase)
                    logger.info(s"digsig $dsig")
                    val payload = s"$id$roles$issueTime$ttl".toLowerCase
                    logger.info(s"payload $payload")

                    if (!CryptoFactory.verifySignature(payload = payload, digSig)
                    ) {
                      logger.debug(s"token signature not match ${digSig} ")
                      sender ! Reply(false, JsString("token invalid"))
                      context.stop(self)
                    } else if (issueTime.toLong + ttl.toLong * 60 < DateFactory.timestamp()) {
                      logger.debug(s"token Expired  ${tok} ")
                      sender ! Reply(false, JsString("token Expired"))
                      context.stop(self)
                    }else {
                      CassandraStore.getAccount(arr2(0), arr2(1)) match {
                        case Some(acc) =>
                          implicit val format: JsonFormat[ConfirmAddPlatform] = jsonFormat5(ConfirmAddPlatform)
                          val trans = Trans(message.id, message.execer, "PlatformActor", "ConfirmAddPlatform", message.toJson.toString, "")
                          CassandraStore.createTrans(trans)
                          CassandraStore.addPlatform(acc.uuid, acc.typ, roles)
                          logger.info(s"platform added $roles")
                          sender ! Reply(true, data=JsString("Platform Added"))
                          context.stop(self)
                      }
                    }
                  }
                case _ =>
                  logger.info(s"invalid token")
                  sender ! Reply(false, JsString("invalid token format"))
                  context.stop(self)
              }
            }catch {
              case any:Any=> logger.error(s"token format invalid $any")
              sender ! Reply(false, JsString("token format invalid"))
            }
          case _ =>
            // no existing account
            logger.error(s"no account found for - ${message.email}")
            sender ! Reply(false, JsString("Acc not registered with VIP"))
        }
      }else{
        logger.error(s"double spend ConfirmAddPlatform $message")
        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)
    case _ =>
      sender ! StatusReply(404, "Method Not Found")
      context.stop(self)
  }


  def checkAccess(execer: String, levels: Set[String]): Option[Account] = {
    logger.debug("Checking access permissions")
    val arr = execer.split(":")
    val acc = CassandraStore.getAccount(arr(0).trim, arr(1).trim)
    val levelsWithAdmin = levels + AccountType.admin.toString
    if (acc.isDefined) {
      logger.debug(s"Found Account with typ: ${acc.get.typ}")
      if (levelsWithAdmin.contains(acc.get.typ)) {
        logger.debug("Permission granted")
        return acc
      }
    }
    logger.debug("Unauthorized request")
    sender ! StatusReply(401, "Unauthorized request")
    context.stop(self)
    None
  }
}
