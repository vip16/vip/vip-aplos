package com.score.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.score.aplos.actor.QualificationActor._
import com.score.aplos.cassandra
import com.score.aplos.cassandra.CassandraStore.createBlob
import com.score.aplos.cassandra.{Account, Blob, CassandraStore, Platform, PlatformStatus, Qualification, Trans, VerifiedStatus}
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf, SmsConf}
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.Criteria
import com.score.aplos.protocol.{QualificationCreateReply, QualificationGetReply, QualificationMessage, QualificationValidityReply, Reply, StatusReply}
import com.score.aplos.redis.RedisStore
import com.score.aplos.util.AppLogger
import spray.json._

import java.text.SimpleDateFormat
import java.util.{Date, UUID}
import scala.collection.mutable.ListBuffer


object QualificationActor {
  val QUALIFICATION_ACTOR_NAME = "QualificationActor"

  case class Create
  (
    messageType: String,
    execer: String,
    id: String,
    typ: String,
    claim: String,
    org_id: String,
    start_date: String,
    end_date: String,
    org_reg_id: String,
    proof_blob: List[String]
  ) extends QualificationMessage

  case class  CreateList
  (
    messageType: String,
    execer: String,
    id: String,
    typ:String,
    entries:List[Entry]
  )extends QualificationMessage

  case class Entry
  (
    user_id:String,  //nic
    claim: String,
    start_date: String,
    end_date: String,
    org_reg_id: String,
    proof_blob: List[String]
  )extends QualificationMessage

  case class Validate
  (
    messageType: String,
    execer: String,
    id: String,
    q_id: String,
    claim: String,
    org_id: String,
    start_date: String,
    end_date: String,
    org_reg_id: String,
    email_typ:String,
    platform_uname:String
  ) extends QualificationMessage


  case class Request
  (
    messageType: String,
    execer: String,
    id: String,
    q_id: String
  ) extends QualificationMessage


  case class Cancel
  (
    messageType: String,
    execer: String,
    id: String,
    q_id: String
  ) extends QualificationMessage

  case class Verify
  (
    messageType: String,
    execer: String,
    id: String,
    q_id: String,
    is_verified: Boolean
  ) extends QualificationMessage


  case class Get
  (
    messageType: String,
    execer: String,
    id: String,
    q_id: String
  ) extends QualificationMessage


  case class GetByUser
  (
    messageType: String,
    execer: String,
    id: String
  ) extends QualificationMessage


  case class GetByOrg
  (
    messageType: String,
    execer: String,
    id: String,
    org_id: String
  ) extends QualificationMessage

  def props()(implicit materializer: ActorMaterializer): Props = Props(new QualificationActor())
}

class QualificationActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with SmsConf with FeatureToggleConf with AppLogger {
  override def receive: Receive = {
    /**
     * get qualifications by id
     * returns a qualification or 404
     */
    case message: Get =>
      logger.debug(s"got qualification Get message $message")
      checkAccess(message.execer, Set(AccountType.user.toString))
      val res = CassandraStore.getQualification(message.q_id)
      var reply: Reply = null
      if (res.isDefined) {
        import com.score.aplos.protocol.QualificationReplyProtocol.qualificationGetReplyFormat
        reply = Reply(true, data = res.get.toJson)
      } else {
        reply = Reply(false, JsString("Qualification dose not exists"))
      }
      sender ! reply
      context.stop(self)

    /**
     * get users own qualifications
     * returns a List of qualifications
     */
    case message: GetByUser =>
      logger.debug(s"got qualification getByOrg message $message")
      val acc: Option[Account] = checkAccess(message.execer, Set(AccountType.user.toString))

      val mustFilter: List[Criteria] = List(Criteria("user_id", acc.get.uuid))
      val (hits, qs) = ElasticStore.getQualifications(mustFilter = mustFilter)
      logger.debug(s"hits: $hits\n Qualifications: $qs")
      import com.score.aplos.protocol.QualificationReplyProtocol.QualificationListReplyFormat
      sender ! Reply(true, data = qs.toJson)
      context.stop(self)

    /**
     * get qualifications send to the org.
     * USER MUST BE a registered verifier of the org.
     * returns a List of qualifications
     */
    case message: GetByOrg =>
      logger.debug(s"got qualification getByOrg message $message")
      checkAccess(message.execer, Set(AccountType.tracer.toString, AccountType.org.toString))

      val mustFilter: List[Criteria] = List(Criteria("org_id", message.org_id))
      val (hits, qs) = ElasticStore.getQualifications(mustFilter = mustFilter)
      logger.debug(s"hits: $hits\n Qualifications: $qs")
      import com.score.aplos.protocol.QualificationReplyProtocol.QualificationListReplyFormat
      sender ! Reply(true, data = qs.toJson)
      context.stop(self)


    /**
     * creating qualification
     * return 200 OK if success
     */
    case message: Create =>
      logger.debug(s"got qualification create message $message")
      // first check double spend
      val id = s"${message.execer};QualificationActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer, "QualificationActor", message.id) && RedisStore.set(id)) {
        val acc: Option[Account] = checkAccess(message.execer, Set(AccountType.user.toString))
        if (acc.isDefined) {
          var blob_ids = List[String]()
          message.proof_blob.foreach(blobString => {
            val blob = createBlob(blobString)
            blob_ids = blob_ids :+ blob.id
          })
          val q = Qualification(
            user_id = acc.get.uuid,
            claim = message.claim,
            org_id = message.org_id,
            org_reg_id = message.org_reg_id,
            typ = message.typ,
            start_date = new SimpleDateFormat("YYYY-MM-DD").parse(message.start_date),
            end_date = new SimpleDateFormat("YYYY-MM-DD").parse(message.end_date),
            proof_blob_id = blob_ids,
            valid_until_date = None,
            verified_time = None,
            verified=VerifiedStatus.notRequested.toString
          )
          import com.score.aplos.protocol.QualificationMessageProtocol._
          val trans = Trans(message.id, message.execer, QUALIFICATION_ACTOR_NAME, "create", message.toJson.toString, "digsig")
          CassandraStore.createTrans(trans)
          CassandraStore.createQualification(q)
          import com.score.aplos.protocol.QualificationReplyProtocol.qualificationCreateReplyFormat
          sender ! Reply(true, data = QualificationCreateReply(q.uuid).toJson)
        } else {
          logger.error(s"no account found for - ${message.execer}")

          sender ! Reply(false, JsString("you are not registered with VIP"))
        }
      }else{
        logger.error(s"double spend activate $message")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)

    /**
     * validate qualification with third party data
     * return 200 OK with body={valid:ture} if success
     * return 200 OK with body={valid:false} if invalid
     */

    case message:CreateList =>
      logger.info(s"got qualification create list message - $message")

      val arr =message.execer.split(":")
      val id = s"${message.execer};QualificationActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer, "QualificationActor", message.id) && RedisStore.set(id)) {
        if (arr(1).equalsIgnoreCase(AccountType.org.toString)||arr(1).equalsIgnoreCase(AccountType.tracer.toString)) {
          CassandraStore.getAccount(arr(0),arr(1)) match {
            case Some(account)=>
              if (account.disabled){
                logger.info(s"account $account disable")
                sender ! Reply(true , data=JsString("account disabled contact admin"))
                context.stop(self)
              }
              message.entries.foreach(
                entry=>{
                  var blob_ids = List[String]()
                  entry.proof_blob.foreach(blobString => {
                    val blob = createBlob(blobString)
                    blob_ids = blob_ids :+ blob.id
                  })
                  val q = Qualification(
                    user_id = entry.user_id,
                    claim = entry.claim,
                    org_id = arr(0),
                    typ = message.typ,
                    start_date = new SimpleDateFormat("YYYY-MM-DD").parse(entry.start_date),
                    end_date = new SimpleDateFormat("YYYY-MM-DD").parse(entry.end_date),
                    proof_blob_id = blob_ids,
                    verified=VerifiedStatus.verified.toString,
                    org_reg_id = entry.org_reg_id,
                    valid_until_date = Option(new SimpleDateFormat("YYYY-MM-DD").parse(entry.end_date)),
                    verified_time = Option(new Date()),
                    verified_by_user_id = arr(0)
                  )
                  CassandraStore.createQualification(q)
                }
              )
              logger.info("createList created")
              sender ! Reply(true,data=JsString("entries added"))
              context.stop(self)

            case None =>
              logger.info(s"acc not found for ${message.execer}")
              sender ! Reply(false,JsString("Not a VIP user"))
              context.stop(self)
          }

        }else{
          logger.info(s"user not authorized ${message.execer}")
          sender ! Reply(false,JsString("Not authorized"))
          context.stop(self)

        }
      }else{
        logger.error(s"double spend deactivate $message")
        sender ! Reply(false, JsString("double spend"))
        context.stop(self)


      }
      context.stop(self)



    case message: Validate =>
      logger.debug(s"got qualification request message $message")
      val arr = message.execer.split(":")
      val email_typ=message.email_typ.split(":")
      val acc = if (arr(1).equalsIgnoreCase("platform")) CassandraStore.getPlatform(arr(0)) else CassandraStore.getAccount(arr(0),arr(1))

      if (acc.isDefined) {
        val qObj = CassandraStore.getQualification(message.q_id)
        if (qObj.isEmpty) {
          sender ! Reply(false, JsString("Qualification dose not exists"))
          context.stop(self)
        }
        val q = qObj.get

        val u = CassandraStore.getAccount(q.user_id, email_typ(1))
        if (u.isEmpty) {
          sender ! Reply(false, JsString("User dose not exists"))
          context.stop(self)
        }
        if (!u.get.platform_id.contains(message.platform_uname + ":" + arr(0))) {
          sender ! Reply(false, JsString("Platform not authorised"))
          context.stop(self)
        }
        if (q.claim.equalsIgnoreCase(message.claim)
          && q.org_id.equalsIgnoreCase(message.org_id)
        ) {
          import com.score.aplos.protocol.QualificationReplyProtocol.qualificationGetReplyFormat
          sender ! Reply(true, data = q.toJson)
          context.stop(self)
        } else {
          sender ! Reply(false, JsString("invalid details"))
          context.stop(self)
        }
      }else{
        sender ! Reply(false, JsString("not authorized"))
        context.stop(self)
      }

    context.stop(self)



    /**
     * set qualification_state to requested state
     * return 200 OK if success
     */
    case message: Request =>
      logger.debug(s"got qualification request message $message")
      //check for double spend
      val id = s"${message.execer};QualificationActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer, "QualificationActor", message.id) && RedisStore.set(id)) {

        val acc: Option[Account] = checkAccess(message.execer, Set(AccountType.user.toString))
        if (acc.isDefined) {
          import com.score.aplos.protocol.QualificationMessageProtocol._
          val trans = Trans(message.id, message.execer, QUALIFICATION_ACTOR_NAME, "request", message.toJson.toString, "digsig")
          CassandraStore.createTrans(trans)

          val q = CassandraStore.getQualification(message.q_id)
          if (q.isEmpty) {
            sender ! Reply(false, JsString("Qualification dose not exists"))
            context.stop(self)
          }
          if (q.get.user_id != acc.get.uuid) {
            sender ! StatusReply(401, "Unauthorized request")
            context.stop(self)
          }
          CassandraStore.updateQualificationVerifiedStatus(
            uuid = message.q_id,
            verified = VerifiedStatus.requested.toString
          )
          sender ! Reply(true, data = JsString("Successfully requested"))
        }else{
          logger.error(s"no account found for - ${message.execer}")
          sender ! Reply(false, JsString("you are not registered with VIP"))
        }
      }else {
        logger.error(s"double spend activate $message")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)


    /**
     * set qualification_state to initial state
     * return 200 OK if success
     */
    case message: Cancel =>
      logger.debug(s"got qualification cancel message $message")
      val id = s"${message.execer};QualificationActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer, "QualificationActor", message.id) && RedisStore.set(id)) {

        val acc: Option[Account] = checkAccess(message.execer, Set(AccountType.user.toString))
        if (acc.isDefined) {

          import com.score.aplos.protocol.QualificationMessageProtocol._
          val trans = Trans(message.id, message.execer, QUALIFICATION_ACTOR_NAME, "cancel", message.toJson.toString, "digsig")
          CassandraStore.createTrans(trans)

          val q = CassandraStore.getQualification(message.q_id)
          if (q.isEmpty) {
            sender ! Reply(false, JsString("Qualification dose not exists"))
            context.stop(self)
          }
          if (q.get.user_id != acc.get.uuid) {
            sender ! StatusReply(401, "Unauthorized request")
            context.stop(self)
          }
          CassandraStore.updateQualificationVerifiedStatus(
            uuid = message.q_id,
            verified = VerifiedStatus.notRequested.toString
          )
          sender ! Reply(true, data = JsString("Successfully requested"))
        }else {
          logger.error(s"no account found for - ${message.execer}")
          sender ! Reply(false, JsString("you are not registered with VIP"))
        }
      }else {
        logger.error(s"double spend activate $message")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)


    /**
     * set qualification verified to verified/rejected state
     * return 200 OK if success
     */
    case message: Verify =>
      logger.debug(s"got qualification verify message $message")
      val id = s"${message.execer};QualificationActor;${message.id}"
      if (!CassandraStore.isDoubleSpend(message.execer, "QualificationActor", message.id) && RedisStore.set(id)) {

        val acc: Option[Account] = checkAccess(message.execer, Set(AccountType.tracer.toString, AccountType.org.toString))
        if (acc.isDefined) {

          import com.score.aplos.protocol.QualificationMessageProtocol._
          val trans = Trans(message.id, message.execer, QUALIFICATION_ACTOR_NAME, "verify", message.toJson.toString, "digsig")
          CassandraStore.createTrans(trans)

          val q = CassandraStore.getQualification(message.q_id)
          if (q.isEmpty) {
            sender ! StatusReply(404, "Qualification dose not exists")
            context.stop(self)
          }
          CassandraStore.updateQualificationVerifiedStatus(
            uuid = message.q_id,
            verified =
              if (message.is_verified) VerifiedStatus.verified.toString
              else VerifiedStatus.rejected.toString,
            verified_time = new Date(),
            verified_by_user_id = acc.get.uuid
          )
          sender ! Reply(true, data = JsString("Successfully requested"))
        }else {
          logger.error(s"no account found for - ${message.execer}")
          sender ! Reply(false, JsString("you are not registered with VIP"))
        }
      }else {
        logger.error(s"double spend activate $message")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)

    case _ =>
      sender ! StatusReply(404, "Method Not Found")
      context.stop(self)
  }


  def checkAccess(execer: String, levels: Set[String], allowPlatform: Boolean = false): Option[Account] = {
    logger.debug("Checking access permissions")
    val arr = execer.split(":")
    arr(1) match {
      case "PLATFORM" =>
        val optionPlatform = CassandraStore.getPlatform(arr(2).trim)
        if (allowPlatform
          && optionPlatform.isDefined
          && optionPlatform.get.status == PlatformStatus.SUBSCRIBED.toString) {
          val platform = optionPlatform.get
          logger.debug(s"Found Platform: ${platform.uuid}, ${platform.name}")

          val acc = CassandraStore.getAccount(arr(0).trim, AccountType.user.toString)
          if (acc.isDefined && acc.get.platform_id.contains(arr(2).trim)) {
            logger.debug(s"Found Account with typ: ${acc.get.typ}")
            logger.debug("Permission granted for ThirdParty")
            return acc
          }
        }

      case _ =>
        val acc = CassandraStore.getAccount(arr(0).trim, arr(1).trim)
        val levelsWithAdmin = levels + AccountType.admin.toString
        if (acc.isDefined) {
          logger.debug(s"Found Account with typ: ${acc.get.typ}")
          if (levelsWithAdmin.contains(acc.get.typ)) {
            logger.debug("Permission granted")
            return acc
          }
        }
    }

    logger.debug("Unauthorized request")
    sender ! StatusReply(401, "Unauthorized request")
    context.stop(self)
    None
  }

}
