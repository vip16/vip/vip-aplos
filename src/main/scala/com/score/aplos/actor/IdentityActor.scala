package com.score.aplos.actor

import akka.actor.{Actor, Props}
import com.datastax.driver.core.utils.UUIDs
import com.score.aplos.config.{ApiConf, AppConf, FeatureToggleConf, SmsConf}
import com.score.aplos.util.{AppLogger, DateFactory}
import com.score.aplos.actor.IdentityActor._
import com.score.aplos.cassandra.CassandraStore.{createBlob, getBlobStrings}
import com.score.aplos.protocol.IdentityMessageProtocol._
import com.score.aplos.protocol.StatusReply
import com.score.aplos.redis.RedisStore

import com.score.aplos.protocol._
import akka.stream.ActorMaterializer
import com.score.aplos.cassandra._
import com.score.aplos.elastic.ElasticStore
import com.score.aplos.elastic.ElasticStore.{Criteria, Page, Sort}

import scala.collection.mutable.ListBuffer
import spray.json._

import scala.util.Try


object IdentityActor {

  case class Create
  (
    messageType: String,
    execer: String,
    id: String,
    email: String,
    typ: String,
    identity_type: String,
    identification_no: String,
    proof_blob: List[String]
  ) extends IdentityMessage //8

  case class Verify
  (
    messageType: String,
    execer: String,
    id: String,
    uuid: String,
    verified: String,
    user: String, //validator id
    org: String //validating org id
  ) extends IdentityMessage //7

  case class Get
  (
    messageType: String, execer: String, id: String,
    uuid: String
  ) extends IdentityMessage //2

  case class Search
  (
    messageType: String, execer: String, id: String,
    email: String,
    typ: String, //USER , ADMIN , TRACER , ORG
    offset: String,
    limit: String,
    identity_typ: String,
    sort: String = "descending"
  ) extends IdentityMessage //9

  case class Remove
  (
    messageType: String, execer: String, id: String,
    email: String,
    typ: String,
    uuid: String
  ) extends IdentityMessage //6

  def props()(implicit materializer: ActorMaterializer): Props = Props(new IdentityActor)
}

class IdentityActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with ApiConf with SmsConf with FeatureToggleConf with AppLogger {
  override def receive: Receive = {
    case create: Create =>
      logger.info(s"got identity create message $create from ${create.execer}")
      val id = s"${create.execer};IdentityActor;${create.id}"
      if (!CassandraStore.isDoubleSpend(create.execer, "IdentityActor", create.id) && RedisStore.set(id)) {

        CassandraStore.getAccount(create.email, create.typ) match {
          case Some(acc) =>
            logger.info(s"account exist")
            if (acc.disabled) {
              logger.info(s"acc is disabled")
              sender ! Reply(false, JsString("account is disabled"))
            }
            else {
              //crete trans and identity
              val trans = Trans(create.id, create.execer, "IdentityActor", "Create", create.toJson.toString, "")
              CassandraStore.createTrans(trans)


              //              create blob
              var blobList = new ListBuffer[String]()
              create.proof_blob.foreach ( blobString =>{
                val blob =createBlob(blobString)
                blobList = blobList :+ blob.id
              })

              val iden_id = UUIDs.random().toString
              val iden = Identity(
                iden_id,
                acc.uuid + ":" + acc.typ,
                create.identity_type,
                create.identification_no,
                VerifiedStatus.notRequested.toString,
                "",
                "",
                blobList.toList
              )
              CassandraStore.createIdentity(iden)
              logger.info(s"identity $iden created for ${create.execer}")
              sender ! Reply(true, data = JsString("identity added to the profile"))
            }
          case None =>
            logger.info(s"no such user ${create.execer}")
            sender ! Reply(false, JsString("Invalid user profile"))
        }
      } else {
        logger.error(s"double spend activate $create")
        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)

    case verify: Verify =>
      logger.info(s"received identity verify message $verify")

      val id = s"${verify.execer};IdentityActor;${verify.id}"
      if (!CassandraStore.isDoubleSpend(verify.execer, "IdentityActor", verify.id) && RedisStore.set(id)) {

        CassandraStore.getAccount(verify.org, AccountType.org.toString) match {
          case Some(acc) =>
            CassandraStore.getAccount(verify.user, AccountType.tracer.toString) match {
              case Some(account) =>
                logger.info(s"account exist")
                if (account.disabled) {
                  logger.info(s"acc is disabled")
                  sender ! Reply(false, JsString("account is disabled"))
                }
                else {
                  val trans = Trans(verify.id, verify.execer, "IdentityActor", "Verify", verify.toJson.toString, "")
                  CassandraStore.createTrans(trans)
                  CassandraStore.verifyIdentity(verify.uuid, verify.verified, verify.user, verify.org)
                  sender ! Reply(true, data = JsString("identity added to the profile"))
                }
              case None =>
                logger.info(s"No such user ${verify.user} with permission ")
                sender ! Reply(false, JsString("No such user"))
            }

          case None =>
            logger.info(s"No such organization ${verify.org} registered ")
            sender ! Reply(false, JsString("No such Organization"))
        }
      } else {
        logger.error(s"double spend verify $verify")

        sender ! Reply(false, JsString("double spend"))
      }
      context.stop(self)

    /**
     * get identity by id
     * returns a identity or 404
     */
    case get: Get =>
      logger.info(s"got get message - $get")
      CassandraStore.getIdentity(get.uuid) match {
        case Some(i) =>
          val blobs = getBlobStrings(i.proof_blobs_id)

          import com.score.aplos.protocol.IdentityReplyProtocol.identityReplyFormat
          sender ! Reply(true, data = IdentityReply(
            i.uuid,
            i.user_id,
            i.identity_type,
            i.identification_no,
            i.verified,
            i.verified_by_user_id,
            i.verified_by_org_id,
            blobs,
            DateFactory.formatToString(Option(i.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse("")
          ).toJson)
        case _ =>
          logger.error(s"no matching identity found - ${get.uuid}")
          sender ! Reply(false, JsString("account is disabled"))
      }
      context.stop(self)

    /**
     * get list of identities.
     * if the typ is user return only the identities of that user
     * if identity_typ is all then return all identifies (for a admin user)
     * for any other identity_typ return all identities belongs to that identity_typ
     * returns a List of identities
     * offset and limit can be used to fetch latest record
     * sort accept ascending or descending (default descending) - sort by time
     */
    case search: Search =>
      logger.info(s"got search message - $search")
      CassandraStore.getAccount(search.email, search.typ) match {
        case Some(a) =>
          val user_id = a.uuid + ":" + a.typ

          // sorts and pages
          val sorts = List(Sort("timestamp", if (search.sort.equalsIgnoreCase("ascending")) true else false)) // sort in descending order by timestamp
          val page = Page(Try(search.offset.toInt).getOrElse(0), Try(search.limit.toInt).getOrElse(10))

          //filters
          var mustFilter = ListBuffer[Criteria]()
          //if normal user only return his identities
          if (search.typ.equalsIgnoreCase(AccountType.user.toString)) {
            mustFilter += Criteria("user_id", user_id)
          }
          if (!search.identity_typ.equalsIgnoreCase("all")) {
            mustFilter += Criteria("identity_type", search.identity_typ)
          }


          val (hits, identities) = ElasticStore.getIdentities(
            List(), List(), mustFilter.toList, List(), None, None, sorts, Option(page))
          val meta = IdentityMeta(search.offset, search.limit, identities.size.toString, hits.toString)
          val searchReply = IdentitySearchReply(meta, identities.map(i =>
            IdentityReply(
              i.uuid,
              i.user_id,
              i.identity_type,
              i.identification_no,
              i.verified,
              i.verified_by_user_id,
              i.verified_by_org_id,
              getBlobStrings(i.proof_blobs_id),
              DateFactory.formatToString(Option(i.timestamp), DateFactory.TIMESTAMP_FORMAT, DateFactory.COLOMBO_TIME_ZONE).getOrElse("")
            )))

          import com.score.aplos.protocol.IdentityReplyProtocol.identitySearchReplyFormat
          sender ! Reply(true, data = searchReply.toJson)
        case None =>
          logger.error(s"unknown user ${search.email}+${search.typ}")
          sender ! StatusReply(401, "Unauthorized request.")
      }
      context.stop(self)

    case remove: Remove =>
      logger.info(s"got search message - $remove")
      CassandraStore.getAccount(remove.email, remove.typ) match {
        case Some(a) =>
          val trans = Trans(remove.id, remove.execer, "IdentityActor", "Remove", remove.toJson.toString, "")
          CassandraStore.createTrans(trans)
          CassandraStore.delIdentity(remove.uuid)
          logger.info(s"${remove.uuid} removed")
          sender ! Reply(true, data = JsString("Removed success"))
        case _ =>
          logger.info(s"user ${remove.email} unknown")
          sender ! StatusReply(401, "Unauthorized request.")

      }
      context.stop(self)

  }
}