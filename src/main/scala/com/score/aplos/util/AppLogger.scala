package com.score.aplos.util

import java.io.{PrintWriter, StringWriter}
import org.slf4j
import org.slf4j.Logger

trait AppLogger {

  val logger: Logger = slf4j.LoggerFactory.getLogger(this.getClass)

  def logError(throwable: Throwable): Unit = {
    val writer = new StringWriter
    throwable.printStackTrace(new PrintWriter(writer))
    logger.error(writer.toString)
  }
}
